package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.dao.UserDao;
import cn.xetsoft.supermarket.entity.User;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @since  2020/12/19 20:46
 * @version 1.0
 * @Description 用户数据库操作测试
 */
class UserDaoImplTest {
    //定义查询操作对象
    UserDao userDao = new UserDaoImpl();

    /**
     * 数据库 测试查询所有
     */
    @org.junit.jupiter.api.Test
    void queryById() {
        try {
            User user = userDao.queryById(1);
            System.out.println( user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 数据库 测试查询所有
     */
    @org.junit.jupiter.api.Test
    void queryAll() {
        try {
            List<User> user = userDao.queryAll();
            System.out.println( user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 数据库 测试登入
     */
    @org.junit.jupiter.api.Test
    void login() {
        try {
            User user = userDao.login("222","222");
            System.out.println( user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 添加测试
     */
    @org.junit.jupiter.api.Test
    void insert() {
        /**
         * 模拟数据
         */
        User user = new User();
        user.setUsername("OKK");
        user.setName("123");
        user.setFrequency("白班");
        user.setRole(Role.ROLE_NO_AUTH);
        user.setPhone("123456789");
        user.setPassword("124587");
        try {
            userDao.insert(user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 数据库更新测试
     */
    @org.junit.jupiter.api.Test
    void update() {
        /**
         * 模拟数据
         */
        User user = new User();
        user.setId(2);
        user.setUsername("OK5K");
        user.setName("123");
        user.setFrequency("白班");
        user.setRole(Role.ROLE_NO_AUTH);
        user.setPhone("123456789");
        user.setPassword("124587");
        /**
         * 测试功能
         */
        try {
            userDao.update(user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 数据库删除测试
     */
    @org.junit.jupiter.api.Test
    void deleteById() {
        try {
            userDao.deleteById(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}