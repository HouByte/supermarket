package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.dao.OrderDao;
import cn.xetsoft.supermarket.entity.Order;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单数据库操作测试
 * @since 2020/12/21 9:23
 */
class OrderDaoImplTest {

    private OrderDao orderDao = new OrderDaoImpl();

    /**
     * 查询id测试
     */
    @Test
    void queryById() {
        try {
            Order order = orderDao.queryById(1);
            System.out.println(order);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 查询所有订单
     */
    @Test
    void queryAll() {
        try {
            List<Order> orders = orderDao.queryAll();
            System.out.println(orders);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 添加订单
     */
    @Test
    void insert() {
        /**
         * 测试数据模拟
         */
        Order order = new Order();
        order.setOrderNo("277716663663");
        order.setDesc("苹果");
        order.setTotalPrice(new BigDecimal(100.0));
        order.setProfit(new BigDecimal(21.00));
        order.setAgent("hhh");
        /**
         * 功能测试
         */
        try {
            orderDao.insert(order);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 更新订单
     */
    @Test
    void update() {
        /**
         * 测试数据模拟
         */
        Order order = new Order();
        order.setId(2);
        order.setOrderNo("277716663663");
        order.setDesc("香蕉s");
        order.setTotalPrice(new BigDecimal(100.2));
        order.setProfit(new BigDecimal(12.2));
        order.setAgent("ooo");
        /**
         * 功能测试
         */
        try {
            int update = orderDao.update(order);
            System.out.println(update);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 订单id删除
     */
    @Test
    void deleteById() {
        try {
            orderDao.deleteById(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}