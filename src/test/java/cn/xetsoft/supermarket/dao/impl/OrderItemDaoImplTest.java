package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.dao.OrderItemDao;
import cn.xetsoft.supermarket.entity.OrderItem;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单商品数据库操作测试
 * @since 2020/12/21 9:23
 */
class OrderItemDaoImplTest {
    private OrderItemDao orderItemDao = new OrderItemDaoImpl();

    /**
     * 查询id测试
     */
    @Test
    void queryById() {
        try {
            OrderItem orderItem = orderItemDao.queryById(1);
            System.out.println(orderItem);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    /**
     * 查询所有测试
     */
    @Test
    void queryAll() {
        try {
            List<OrderItem> orderItems = orderItemDao.queryAll();
            System.out.println(orderItems);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 添加测试
     */
    @Test
    void insert() {
        /**
         * 测试数据
         */
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderNo("123456");
        orderItem.setSalePrice(new BigDecimal(12.2));
        orderItem.setGoodname("苹果");
        orderItem.setGoodbrand("富士山");
        orderItem.setType("kg");
        orderItem.setQuantity(11);
        try {
            orderItemDao.insert(orderItem);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 更新测试
     */
    @Test
    void update() {
        /**
         * 测试数据
         */
        OrderItem orderItem = new OrderItem();
        orderItem.setId(1);
        orderItem.setOrderNo("123456");
        orderItem.setSalePrice(new BigDecimal(12.2));
        orderItem.setGoodname("青苹果");
        orderItem.setGoodbrand("富士山");
        orderItem.setType("kg");
        orderItem.setQuantity(11);
        try {
            orderItemDao.update(orderItem);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 删除测试
     */
    @Test
    void deleteById() {
        try {
            orderItemDao.deleteById(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}