package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.dao.GoodDao;
import cn.xetsoft.supermarket.entity.Good;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 商品表数据库操作测试
 * @since 2020/12/20 18:51
 */
class GoodDaoImplTest {

    //商品表数据库操作类创建对象
    GoodDao goodDao = new GoodDaoImpl();

    @Test
    void queryById() {
        try {
            Good apple = goodDao.queryById(1);
            System.out.println(apple);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    void queryByGoodname() {
        try {
            List<Good> apple = goodDao.queryByLikeGoodnameAndGoodbrand("苹果");
            System.out.println(apple);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    void queryAll() {
        try {
            List<Good> apple = goodDao.queryAll();
            System.out.println(apple);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    void insert() {
        /**
         * 模拟数据
         */
        Good good = new Good();
        good.setGoodname("香蕉");
        good.setGoodbrand("富士山");
        good.setInprice(new BigDecimal(5.5));
        good.setSaleprice(new BigDecimal(6.6));
        good.setStock(12);
        good.setIndate(new Date());
        good.setAgent("刘");
        good.setType("kg");
        /**
         * 测试功能
         */
        try {
            goodDao.insert(good);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    void update() {
        /**
         * 模拟数据
         */
        Good good = new Good();
        good.setGid(1);
        good.setGoodname("青苹果");
        good.setGoodbrand("富士山");
        good.setInprice(new BigDecimal(5.5));
        good.setSaleprice(new BigDecimal(6.6));
        good.setStock(12);
        good.setAgent("刘");
        good.setType("kg");
        /**
         * 测试功能
         */
        try {
            goodDao.update(good);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    void deleteById() {
        try {
            goodDao.deleteById(3);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}