package cn.xetsoft.supermarket.util;


import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @Author 侯国强
 * @Date 2020/12/8 17:19
 * @Version 1.0
 * @Description 测试jdbc工具
 */


public class JDBCUtilTest {
    public static void main(String[] args) throws Exception {
//        // 加载驱动
//        Class.forName("com.mysql.jdbc.Driver");
//        // 获取连接对象
//        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/market", "root", "root");
//        // 定义sql 语句
//        String sql = "select * from user";
//        // 创建执行对象
//        Statement statement = connection.createStatement();
//        // 执行查询sql语句
//        ResultSet resultSet = statement.executeQuery(sql);
//        // 输出结果集
//        //如果有数据，rs.next()返回true
//        while(resultSet.next()){
//            System.out.println(resultSet.getString("username"));
//        }
//        JDBCUtils.getInstanceConnection();
//        if (JDBCUtils.isDbEnable()){
//            System.out.println("333");
//        }
//        JDBCUtils.existTable("sds");

        //当前项目下路径
        File file = new File("");
        String filePath = file.getCanonicalPath();
        System.out.println(filePath);

        //当前项目下xml文件夹
        File file1 = new File("");
        String filePath1 = file1.getCanonicalPath()+File.separator+"xml\\";
        System.out.println(filePath1);

        //获取类加载的根路径
        File file3 = new File(JDBCUtils.class.getResource("/").getPath());
        System.out.println(file3);

        //获取当前类的所在工程路径
        String url1 = JDBCUtils.class.getClassLoader().getResource("").toString();
        System.out.println(url1);
    }
}
