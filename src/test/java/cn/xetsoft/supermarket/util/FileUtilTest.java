package cn.xetsoft.supermarket.util;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description
 * @since 2020/12/25 20:01
 */
class FileUtilTest {

    @Test
    void isFile() {
    }

    @Test
    void readFile() {
        try {
            File file = new File("logs");
            File[] files = file.listFiles();
            String filename = files[files.length-1].toString();
            String s = FileUtil.readLastRows(filename,null,100);
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}