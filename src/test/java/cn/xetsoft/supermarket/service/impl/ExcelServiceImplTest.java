package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.service.ExcelService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description
 * @since 2020/12/21 20:00
 */
class ExcelServiceImplTest {

    private ExcelService excelService = new ExcelServiceImpl();
    @Test
    void writeGoodExcel() {
        List<Good> goods = new ArrayList<Good>();
        for (int i = 0; i < 10; i++) {
            Good good = new Good();
            good.setGid(i);
            good.setGoodname("香蕉"+1);
            good.setGoodbrand("富士山");
            good.setInprice(new BigDecimal(5.5));
            good.setSaleprice(new BigDecimal(6.6));
            good.setIntime(new Date());
            good.setSaletime(new Date());
            good.setStock(12);
            good.setAgent("刘");
            good.setType("kg");
            good.setCreateTime(new Date());
            good.setUpdateTime(good.getCreateTime());
            goods.add(good);
        }

        excelService.writeGoodExcel(goods,"");
    }

    @Test
    void writeOrderExcel() {
    }

    @Test
    void writeOrderItemExcel() {
    }


}