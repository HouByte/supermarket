package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.dao.impl.GoodDaoImpl;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.service.GoodService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 商品服务层测试
 * @since 2020/12/20 19:53
 */
@Slf4j
class GoodServiceImplTest {
    //商品表数据库操作创建对象
    GoodService goodService = new GoodServiceImpl(new GoodDaoImpl());

    /**
     * 通过id查询商品服务测试
     */
    @Test
    void queryById() {
        //调用通过id查询商品，得到数据代理类
        DataResponse<Good> goodDataResponse = goodService.queryById(1);
        //通过代理类判断是否成功
        if (goodDataResponse.isSuccess()){//操作成功
            Good data = goodDataResponse.getData();
            System.out.println(data);
        } else {
            //不成功打印错误信息
            System.out.println(goodDataResponse.getMsg());
        }
    }

    /**
     * 通过商品名称关键字查询商品服务测试
     */
    @Test
    void queryByGoodname() {
        //通过商品名称关键字查询商品，得到数据代理类
        DataResponse<List<Good>> goodDataResponse = goodService.queryByGoodnameAndGoodbrand("牙刷");
        //通过代理类判断是否成功
        if (goodDataResponse.isSuccess()){//操作成功
            List<Good> data = goodDataResponse.getData();
            log.info(data.toString());
        } else {
            //不成功打印错误信息
            System.out.println(goodDataResponse.getMsg());
        }
    }

    /**
     * 查询所有商品测试
     */
    @Test
    void queryAll() {
        //查询所有商品，得到数据代理类
        DataResponse<List<Good>> goodDataResponse = goodService.queryAll();
        //通过代理类判断是否成功
        if (goodDataResponse.isSuccess()){//操作成功
            List<Good> data = goodDataResponse.getData();
            for (int i = 0; i < 10000; i++) {
                log.debug(data.toString());
            }

        } else {
            //不成功打印错误信息
            System.out.println(goodDataResponse.getMsg());
        }
    }

    /**
     * 插入商品测试
     */
    @Test
    void insert() {
        //设置测试数据
        Good good = new Good();
        good.setGoodname("香蕉");
        good.setGoodbrand("富士山");
        good.setInprice(new BigDecimal(5.5));
        good.setSaleprice(new BigDecimal(6.6));
        good.setStock(12);
        good.setAgent("刘");
        good.setType("kg");
        /**
         * 测试功能
         */
        //添加商品，得到数据代理类
        DataResponse insert = goodService.insert(good);
        if (insert.isSuccess()){
            System.out.println("添加成功");
        } else {
            System.out.println("添加失败");
        }
    }

    /**
     * 更新服务测试
     */
    @Test
    void update() {
        //设置测试数据
        Good good = new Good();
        good.setGid(2);
        good.setGoodname("香蕉");
        good.setGoodbrand("富士山");
        good.setInprice(new BigDecimal(5.5));
        good.setSaleprice(new BigDecimal(6.6));
        good.setStock(12);
        good.setAgent("刘");
        good.setType("kg");
        /**
         * 测试功能
         */
        //添加商品，得到数据代理类
        DataResponse insert = goodService.insert(good);
        if (insert.isSuccess()){
            System.out.println("添加成功");
        } else {
            System.out.println("添加失败");
        }
    }

    /**
     * 删除服务测试
     */
    @Test
    void deleteById() {
        DataResponse response = goodService.deleteById(4);
        if (response.isSuccess()){
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }
    }
}