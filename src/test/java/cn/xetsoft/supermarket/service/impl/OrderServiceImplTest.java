package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.dao.impl.OrderDaoImpl;
import cn.xetsoft.supermarket.dao.impl.OrderItemDaoImpl;
import cn.xetsoft.supermarket.entity.Cart;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.OrderService;
import cn.xetsoft.supermarket.vo.OrderVo;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description
 * @since 2020/12/21 15:35
 */
class OrderServiceImplTest {

    //订单服务创建
    OrderService orderService = Bean.getOrderServiceInstance();

    @Test
    void queryById(){
        DataResponse<Order> response = orderService.queryById(2);
        System.out.println(response);
    }

    @Test
    void queryByOrderNo() {
        DataResponse<Order> response = orderService.queryByOrderNo("277716663663");
        System.out.println(response);
    }

    @Test
    void queryByDate() {
        DataResponse<OrderVo> response = orderService.queryByDate("2020-12-21");
        System.out.println(response);
    }

    @Test
    void queryByToday() {
        DataResponse<OrderVo> response = orderService.queryByToday();
        System.out.println(response);
    }

    @Test
    void queryByNowMonth() {
        DataResponse<OrderVo> response = orderService.queryByNowMonth();
        System.out.println(response);
    }

    @Test
    void queryAll() {
        DataResponse<OrderVo> response = orderService.queryAll();
        System.out.println(response);
    }

    @Test
    void insert() {
        /**
         * 测试数据模拟
         */
        Cart cart = new Cart();
        cart.setGoodbrand("富士山");
        cart.setAgent("hhh");
        cart.setInprice(new BigDecimal(4.4));
        cart.setGoodname("青苹果");
        cart.setQuantity(12);
        cart.setSaleprice(new BigDecimal(5.6));
        cart.setType("kg");
        /**
         * 测试功能
         */
        System.out.println("插入"+cart);
        List<Cart> carts = new ArrayList<Cart>();
        carts.add(cart);
        DataResponse<OrderVo> response = orderService.insert(carts);
        System.out.println(response);
    }

    @Test
    void update() {
        /**
         * 测试数据模拟
         */
        Order order = new Order();
        order.setId(23);
        order.setOrderNo("277716663663");
        order.setDesc("香蕉");
        order.setTotalPrice(new BigDecimal(100.2));
        order.setProfit(new BigDecimal(12.2));
        order.setAgent("ooo");
        /**
         * 功能测试
         */
        DataResponse<OrderVo> response = orderService.update(order);
        System.out.println(response);
    }

    @Test
    void deleteById() {
        DataResponse<OrderVo> response = orderService.deleteById(23);
        System.out.println(response);
    }
}