package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.dao.impl.DbDaoImpl;
import cn.xetsoft.supermarket.entity.ExportSelect;
import cn.xetsoft.supermarket.service.SettingService;
import org.junit.jupiter.api.Test;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description
 * @since 2020/12/22 19:46
 */
class SettingServiceImplTest {

    private SettingService settingService = new SettingServiceImpl(new DbDaoImpl());
    @Test
    void initDatabase() {
        settingService.initDatabase(true );
    }

    @Test
    void initGoodData() {

        DataResponse response = settingService.initOrderData("熵客云超市数据1.xlsx");
        System.out.println(response);
    }

    @Test
    void initOrderData() {
    }

    @Test
    void initOrderItemData() {
    }

    @Test
    void initUserData() {
        settingService.initUserData("熵客云超市数据(init).xlsx");
    }

    @Test
    void createDataTemplate() {
        settingService.createDataTemplate("熵客云超市导入模板.xlsx");
    }


    @Test
    void exportDbData() {
        settingService.exportDbData("数据信息.xlsx", Role.ROLE_ADMIN,new ExportSelect(true,true,true));
    }
}