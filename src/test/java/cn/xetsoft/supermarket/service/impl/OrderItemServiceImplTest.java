package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.dao.impl.OrderItemDaoImpl;
import cn.xetsoft.supermarket.entity.OrderItem;
import cn.xetsoft.supermarket.service.OrderItemService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description
 * @since 2020/12/21 18:26
 */
class OrderItemServiceImplTest {

    OrderItemService orderItemService = new OrderItemServiceImpl(new OrderItemDaoImpl());

    /**
     * 查询Id测试
     */
    @Test
    void queryById() {
    }

    /**
     * 查询所有测试
     */
    @Test
    void queryAll() {
        DataResponse<List<OrderItem>> listDataResponse = orderItemService.queryAll();
        System.out.println(listDataResponse);
    }

    /**
     * 查询所有测试
     */
    @Test
    void queryByOrderNo() {
        DataResponse<List<OrderItem>> listDataResponse = orderItemService.queryByOrderNo("2012262017250760");
        System.out.println(listDataResponse);
    }

    /**
     * 添加测试
     */
    @Test
    void insert() {
    }

    /**
     * 更新测试
     */
    @Test
    void update() {
    }

    /**
     * 删除测试
     */
    @Test
    void deleteById() {
    }
}