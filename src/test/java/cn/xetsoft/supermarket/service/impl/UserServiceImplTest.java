package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.dao.DbDao;
import cn.xetsoft.supermarket.dao.impl.DbDaoImpl;
import cn.xetsoft.supermarket.dao.impl.UserDaoImpl;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.util.JDBCUtils;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 用户服务层测试
 * @since 2020/12/20 20:15
 */
class UserServiceImplTest {
    //用户表数据库操作创建对象
    private UserService userService = new UserServiceImpl(new UserDaoImpl());

    /**
     * 调用通过id查询商品，得到数据代理类
     */
    @Test
    void queryById() {
        //调用通过id查询商品，得到数据代理类
        DataResponse<User> userDataResponse = userService.queryById(1);
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println(userDataResponse.getData());
        } else { //失败查看错误信息
            System.out.println(userDataResponse.getMsg());
        }
    }

    /**
     * 查询所有用户测试
     */
    @Test
    void queryAll() {
        //调用查询所有用户，得到数据代理类
        DataResponse<User> userDataResponse = userService.queryById(1);
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println(userDataResponse.getData());
        } else { //失败查看错误信息
            System.out.println(userDataResponse.getMsg());
        }
    }

    /**
     * 登入服务测试
     */
    @Test
    void login() {
        //调用登入服务，得到数据代理类
        DataResponse<User> userDataResponse = userService.login("222","222");
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println(userDataResponse.getData());
        } else { //失败查看错误信息
            System.out.println(userDataResponse.getMsg());
        }
    }

    /**
     * 注册服务测试
     */
    @Test
    void register() {
        /**
         * 模拟数据
         */
        User user = new User();
        user.setName("root");
        user.setUsername("root");
        user.setFrequency("白班");
        user.setRole(Role.ROLE_NO_AUTH);
        user.setPhone("12345678901");
        user.setPassword("root");
        /**
         * 测试功能
         */
        //调用查询所有商品，得到数据代理类
        DataResponse userDataResponse = userService.register(user);
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println("【成功】:"+userDataResponse.getMsg());
        } else { //失败查看错误信息
            System.out.println("【失败】:"+userDataResponse.getMsg());
        }
    }

    /**
     * 更新服务测试
     */
    @Test
    void update() {
        /**
         * 模拟数据
         */
        User user = new User();
        user.setId(2);
        user.setUsername("opp");
        user.setName("opp");
        user.setFrequency("白班");
        user.setRole(Role.ROLE_NO_AUTH);
        user.setPhone("12345678901");
        user.setPassword("opp");
        /**
         * 测试功能
         */
        //调用更新，得到数据代理类
        DataResponse userDataResponse = userService.update(user);
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println("【成功】:"+userDataResponse.getMsg());
        } else { //失败查看错误信息
            System.out.println("【失败】:"+userDataResponse.getMsg());
        }
    }

    /**
     * 删除服务测试
     */
    @Test
    void deleteById() {

        DbDao dbDao = new DbDaoImpl();
//        try {
//            //int i = dbDao.useDatabase(JDBCUtils.getDbConfig().getDatabase());
//            //System.out.println("--------"+i+"----------");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }

        //调用更新，得到数据代理类
        DataResponse userDataResponse = userService.deleteById(8);
        if (userDataResponse.isSuccess()){//成功，显示结果数据
            System.out.println("【成功】:"+userDataResponse.getMsg());
        } else { //失败查看错误信息
            System.out.println("【失败】:"+userDataResponse.getMsg());
        }
    }
}