package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.Good;
import java.util.List;

/**
 * @author 侯国强
 * @since 2020/12/19 18:19
 * @version 1.0
 * @Description (Good)表服务接口
 */
public interface GoodService {

    /**
     * 通过ID查询单条数据
     *
     * @param gid 主键
     * @return 实例对象
     */
    DataResponse<Good> queryById(Integer gid);

    /**
     * 通过商品名/品牌名称模糊查询多条数据
     * @param keyword 关键字
     * @return 实例对象
     */
    DataResponse<List<Good>> queryByGoodnameAndGoodbrand(String keyword);

    /**
     * 通过商品名查询单条数据
     * @param goodname 关键字
     * @return 实例对象
     */
    DataResponse<Good> queryByGoodname(String goodname);

    /**
     * 查询多条数据
     * @return 对象列表
     */
    DataResponse<List<Good>> queryAll();

    /**
     * 新增数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    DataResponse<Good> insert(Good good);

    /**
     * 修改数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    DataResponse<Good> update(Good good);

    /**
     * 通过主键删除数据
     *
     * @param gid 主键
     * @return 是否成功
     */
    DataResponse deleteById(Integer gid);

}