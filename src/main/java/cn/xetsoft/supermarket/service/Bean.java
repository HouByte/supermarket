package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.dao.impl.*;
import cn.xetsoft.supermarket.service.impl.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 对象创建管理类
 * @since 2020/12/21 21:18
 */
@Slf4j //使用lombok日志的注解
public class Bean {

    //用户服务
    private static UserService userService = null;

    //商品服务
    private static GoodService goodService = null;

    //订单服务
    private static OrderService orderService = null;

    //订单商品
    private static OrderItemService orderItemService = null;

    //Excel 服务
    private static ExcelService excelService = null;

    //设置服务
    private static SettingService settingService = null;

    private Bean() {
    }

    /**
     * 创建用户服务
     * @return
     */
    public synchronized static UserService getUserServiceInstance(){
        //懒汉模式
        if (userService == null){
            log.debug("创建 UserService 实例: new UserServiceImpl(new UserDaoImpl())");
            //创建用户服务并传入需要的数据库操作
            userService = new UserServiceImpl(new UserDaoImpl());
        }
        return userService;
    }

    /**
     * 创建商品服务
     * @return
     */
    public synchronized static GoodService getGoodServiceInstance(){
        //懒汉模式
        if (goodService == null){
            log.debug("创建 GoodService 实例: new GoodServiceImpl(new GoodDaoImpl())");
            //创建商品服务并传入需要的数据库操作
            goodService = new GoodServiceImpl(new GoodDaoImpl());
        }
        return goodService;
    }

    /**
     * 创建用户服务
     * @return
     */
    public synchronized static OrderService getOrderServiceInstance(){
        //懒汉模式
        if (orderService == null){
            log.debug("创建 OrderService 实例: new OrderServiceImpl(new OrderDaoImpl(),new OrderItemServiceImpl(new OrderItemDaoImpl()))");
            //创建订单服务并传入需要的数据库操作
            orderService = new OrderServiceImpl(new OrderDaoImpl(),new OrderItemServiceImpl(new OrderItemDaoImpl()));
        }
        return orderService;
    }

    /**
     * 创建用户服务
     * @return
     */
    public synchronized static OrderItemService getOrderItemServiceInstance(){
        //懒汉模式
        if (orderItemService == null){
            log.debug("创建 OrderItemService 实例: new OrderItemServiceImpl(new OrderItemDaoImpl())");
            //创建订单商品服务并传入需要的数据库操作
            orderItemService = new OrderItemServiceImpl(new OrderItemDaoImpl());
        }
        return orderItemService;
    }

    /**
     * 创建用户服务
     * @return
     */
    public synchronized static ExcelService getExcelServiceInstance(){
        //懒汉模式
        if (excelService == null){
            log.debug("创建 ExcelService 实例: new ExcelServiceImpl()");
            //创建Excel服务并传入需要的数据库操作
            excelService = new ExcelServiceImpl();
        }
        return excelService;
    }

    /**
     * 创建设置服务
     * @return
     */
    public synchronized static SettingService getSettingServiceInstance(){
        //懒汉模式
        if (settingService == null){
            log.debug("创建 SettingService 实例: new SettingServiceImpl(new DbDaoImpl())");
            //创建设置服务并传入需要的数据库操作
            settingService = new SettingServiceImpl(new DbDaoImpl());
        }
        return settingService;
    }
}
