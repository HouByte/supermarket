package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.entity.OrderItem;
import cn.xetsoft.supermarket.entity.User;

import java.util.List;

/**
 * @author Vincent Vic
 * @version 1.0
 * @Description 使用阿里巴巴提供的EasyExcel读写Excel
 * @since 2020/12/21 19:08
 */
public interface ExcelService {

    /**
     * 导出数据库所有内容excel
     * @param goods
     * @param filename
     * @return
     */
    DataResponse writeAllExcel(List<Good> goods,List<Order> orders,List<OrderItem> orderItems,List<User> users,String filename);

    /**
     * 生成模板excel
     * @param filename
     * @return
     */
    DataResponse writeTemplateExcel(String filename);

    /**
     * 导出商品excel
     * @param goods
     * @param filename
     * @return
     */
    DataResponse writeGoodExcel(List<Good> goods,String filename);

    /**
     * 导出订单excel 包含订单商品
     */
    DataResponse writeOrderExcel(List<Order> orders,List<OrderItem> orderItems,String filename);



    /**
     * 导出用户excel
     * @param users
     * @return
     */
    DataResponse writeUserExcel(List<User> users,String filename);


    /**
     * 读取初始化商品数据表
     * @param fileName
     * @return
     */
    List<Good> readGoodExcel(String fileName);

    /**
     * 读取初始化订单数据表
     * @param fileName
     * @return
     */
    List<Order> readOrderExcel(String fileName);

    /**
     * 读取初始化订单商品数据表
     * @param fileName
     * @return
     */
    List<OrderItem> readOrderItemExcel(String fileName);

    /**
     * 读取初始化用户数据表
     * @param fileName
     * @return
     */
    List<User> readUserExcel(String fileName);
}
