package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.User;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/19 18:19
 * @version 1.0
 * @Description (User)表服务接口
 */
public interface UserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    DataResponse<User> queryById(Integer id);

    /**
     * 通过username查询单条数据
     *
     * @param username 用户名
     * @return 实例对象
     */
    DataResponse<User> queryByUsername(String username);

    /**
     * 查询多条数据
     * @return 对象列表
     */
    DataResponse<List<User>> queryAll();

    /**
     * 登入服务
     * @param username 用户名
     * @param password 密码
     * @return
     */
    DataResponse<User> login(String username,String password);
    /**
     * 用户注册 - 新增数据
     * @param user 实例对象
     * @return 实例对象
     */
    DataResponse register(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    DataResponse<User> update(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    DataResponse deleteById(Integer id);

}