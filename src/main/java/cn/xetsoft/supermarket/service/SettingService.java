package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.entity.ExportSelect;
import cn.xetsoft.supermarket.util.DbConfig;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 初始化服务
 * @since 2020/12/21 20:58
 */
public interface SettingService {


    /**
     * 初始化系统
     *  4041 初始化检测数据库不存在
     *  4042 初始化检测数据表不存在
     *  建议调用数据库设置页面初始化页面
     *  500 异常
     */
    DataResponse initSystem();
    /**
     * 初始化数据库
     * @param isClear 是否清库
     */
    DataResponse initDatabase(boolean isClear);

    /**
     * 初始化数据库配置
     * @param dbConfig 数据库配置，null默认
     */
    DataResponse initDbConfig(DbConfig dbConfig);

    /**
     * 使用excel 初始化商品数据表数据
     */
    DataResponse initGoodData(String filename);

    /**
     * 使用excel 初始化订单数据表数据
     */
    DataResponse initOrderData(String filename);

    /**
     * 使用excel 初始化订单商品数据表数据
     */
    DataResponse initOrderItemData(String filename);

    /**
     * 使用excel 初始化用户数据表数据
     * 密码默认：123456
     */
    DataResponse initUserData(String filename);

    /**
     * 导出数据库数据
     *  @param filename 导出文件名
     * @param role 角色 非管理员不能导出用户
     * @param exportSelect
     */
    DataResponse exportDbData(String filename, Role role, ExportSelect exportSelect);

    /**
     * 创建excel模板
     * @param filename 模板文件名
     */
    DataResponse createDataTemplate(String filename);

    /**
     * 查看日志最后几行
     */
    DataResponse tailLogInfo(int row);
}
