package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.dao.UserDao;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.util.RegexUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/19 18:20
 * @version 1.0
 * @Description (User)表服务实现类
 */

@Slf4j //使用lombok日志的注解
public class UserServiceImpl implements UserService {

    //数据库操作类
    private UserDao userDao;

    //需要外部提供实现类操作
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public DataResponse<User> queryById(Integer id) {
        //健壮性判断
        if (null == id){
            //日志警告提示
            log.warn("查询id不不能为空");
            return DataResponse.error("查询id不不能为空");
        }
        try {
            //调用dao数据库操作查询id
            User user = this.userDao.queryById(id);
            //判断是否操作
            if(null == user){
                //日志警告提示
                log.warn("用户不存在");
                return DataResponse.error("用户不存在");
            }
            return DataResponse.success(user);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 通过username查询单条数据
     *
     * @param username 用户名
     * @return 实例对象
     */
    @Override
    public DataResponse<User> queryByUsername(String username) {
        //健壮性判断
        if (null == username){
            //日志警告提示
            log.warn("查询username不不能为空");
            return DataResponse.error("查询username不不能为空");
        }
        try {
            //调用dao数据库操作查询username
            User user = this.userDao.queryByUserName(username);
            //判断是否操作
            if(null == user){
                //日志警告提示
                log.warn("用户不存在");
                return DataResponse.error("用户不存在");
            }
            return DataResponse.success(user);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 查询多条数据
     * @return 对象列表
     */
    @Override
    public DataResponse<List<User>> queryAll() {
        try {
            //调用dao数据库操作查询id
            List<User> users = this.userDao.queryAll();
            //判断是否操作
            if(users.size()==0){
                //日志警告提示
                log.warn("没有用户");
                return DataResponse.error("没有用户");
            }
            return DataResponse.success(users);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 登入服务
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public DataResponse<User> login(String username, String password) {
        if (null == username){
            //日志警告提示
            log.warn("用户名为空");
            return DataResponse.error("用户名为空");
        }
        if (null == password){
            //日志警告提示
            log.warn("密码为空");
            return DataResponse.error("密码为空");
        }
        try {
            //password MD5
            password = DigestUtils.md5Hex(password.getBytes());
            User login = userDao.login(username, password);
            if (null == login){
                //日志警告提示
                log.warn("用户不存在或者密码错误");
                return DataResponse.error("用户不存在或者密码错误");
            }
            return DataResponse.success(login);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
    }


    /**
     * 用户注册 - 新增数据
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse register(User user) {
        //健壮性判断
        if (null == user){
            //日志警告提示
            log.warn("注册信息为空");
            return DataResponse.error("注册信息为空");
        }
        DataResponse response = isUserNullable(user,false);
        if (response != null) {
            //日志警告提示
            log.warn("校验数据错误 -> {}",response.getMsg());
            return response;
        }

        try {
            //检查用户名是否重复
            User isUser = userDao.queryByUserName(user.getUsername());
            if(null != isUser){
                //日志警告提示
                log.warn("用户名已存在");
                return DataResponse.error("用户名已存在");
            }
            //密码经过md5保存
            String newPassword = DigestUtils.md5Hex(user.getPassword().getBytes());
            user.setPassword(newPassword);
            int insert = this.userDao.insert(user);
            if (insert <= 0){
                //日志警告提示
                log.warn("插入失败");
                return DataResponse.error("插入失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
        return DataResponse.success("注册成功");
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse<User> update(User user) {
        //健壮性判断
        if (null == user){
            //日志警告提示
            log.warn("更新信息为空");
            //通过代理类传递失败消息
            return DataResponse.error("更新信息为空");
        }
        //判断主键是否存在
        if (user.getId() == null){
            //日志警告提示
            log.warn("id不不能为空");
            //通过代理类传递失败消息
            return DataResponse.error("id不不能为空");
        }
        //判断其他数据健壮性
        DataResponse response = isUserNullable(user,true);
        if (response != null) {
            //日志警告提示
            log.warn("校验数据错误 -> {}",response.getMsg());
            return response;
        }
        try {
            //查询是否存在
            User user1 = this.userDao.queryById(user.getId());
            if (null == user1){
                //日志警告提示
                log.warn("用户不存在");
                return DataResponse.error("用户不存在");
            }
            int update = this.userDao.update(user);
            if (update == 0){
                //日志警告提示
                log.warn("更新失败");
                return DataResponse.error("更新失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
        return this.queryById(user.getId());
    }

    /**
     * 非主键健壮性判断
     * 检测数据是否存在空值
     * @param user
     * @return
     */
    private DataResponse<Object> isUserNullable(User user,boolean isUpdate) {
        /**
         * 判断是否为null
         * 字符串判断去空格之后是否为空
         */
        if (user.getUsername() == null || user.getUsername().trim().compareTo("")==0){
            return DataResponse.error("用户名不能为空");//通过代理类传递失败消息
        }
        if (user.getPassword() == null || user.getPassword().trim().compareTo("")==0){
            return DataResponse.error("密码不能为空");//通过代理类传递失败消息
        }
        if (user.getPhone() == null || user.getPhone().trim().compareTo("")==0){
            return DataResponse.error("手机号不能为空");//通过代理类传递失败消息
        }
        if (!RegexUtils.validateMobilePhone(user.getPhone())){
            return DataResponse.error("手机号不合法");//通过代理类传递失败消息
        }
        if (user.getName() == null || user.getName().trim().compareTo("")==0){
            return DataResponse.error("姓名不能为空");//通过代理类传递失败消息
        }
        /**
         * 插入不需要班次和角色，需要排除
         */
        if (!isUpdate){
            return null;
        }
        if (user.getFrequency() == null || user.getFrequency().trim().compareTo("")==0){
            return DataResponse.error("班次不能为空");//通过代理类传递失败消息
        }
        if (user.getRole() == null){
            return DataResponse.error("角色不能为空");//通过代理类传递失败消息
        }
        return null;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public DataResponse deleteById(Integer id) {
        //健壮性判断
        if (null == id){
            //日志警告提示
            log.warn("查询id不不能为空");
            return DataResponse.error("查询id不不能为空");
        }
        try {
            //查询是否存在
            User user = this.userDao.queryById(id);
            if (null == user){
                //日志警告提示
                log.warn("用户不存在");
                return DataResponse.error("用户不存在");
            }
            int deleteById = this.userDao.deleteById(id);
            if (deleteById <= 0){
                //日志警告提示
                log.warn("删除失败");
                return DataResponse.error("删除失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            return DataResponse.errorException(throwables.getMessage());
        }
        return DataResponse.success("删除成功");
    }
}