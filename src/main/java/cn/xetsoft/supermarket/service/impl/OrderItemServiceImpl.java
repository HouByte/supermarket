package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.OrderItem;
import cn.xetsoft.supermarket.dao.OrderItemDao;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.OrderItemService;
import lombok.extern.slf4j.Slf4j;


import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (OrderItem)表服务实现类
 */

@Slf4j //使用lombok日志的注解
public class OrderItemServiceImpl implements OrderItemService {

    private OrderItemDao orderItemDao;

    public OrderItemServiceImpl(OrderItemDao orderItemDao) {
        this.orderItemDao = orderItemDao;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public DataResponse<OrderItem> queryById(Integer id) {
        //健壮性判断
        if (null == id){
            //日志警告提示
            log.warn("订单id为空");
            return DataResponse.error("ID不能为空");
        }
        try {
            OrderItem orderItem = this.orderItemDao.queryById(id);
            if (null == orderItem){
                //日志警告提示
                log.warn("订单商品不存在");
                return DataResponse.error("订单商品不存在");
            }
            return DataResponse.success("成功",orderItem);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 查询所有
     * @return 实例对象
     */
    @Override
    public DataResponse<List<OrderItem>> queryAll() {
        try {
            List<OrderItem> orderItems = this.orderItemDao.queryAll();
            if (orderItems.size() == 0){
                //日志警告提示
                log.warn("订单商品列表不存在");
                return DataResponse.error("订单商品列表不存在");
            }
            return DataResponse.success("成功",orderItems);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 通过订单号查询
     *
     * @param orderNo
     * @return 实例对象
     */
    @Override
    public DataResponse<List<OrderItem>> queryByOrderNo(String orderNo) {
        try {
            List<OrderItem> orderItems = this.orderItemDao.queryByOrderNo(orderNo);
            if (orderItems.size() == 0){
                //日志警告提示
                log.warn("订单商品列表不存在");
                return DataResponse.error("订单商品列表不存在");
            }
            return DataResponse.success("成功",orderItems);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse<OrderItem> insert(OrderItem orderItem) {
        //健壮性判断
        if (null == orderItem){
            //日志警告提示
            log.warn("订单商品为空");
            return DataResponse.error("订单商品不能为空");
        }
        //校验数据
        DataResponse<OrderItem> response = ckeckOrderItem(orderItem);
        if (response != null) {
            //日志警告提示
            log.warn("校验不通过 -> {}",response.getMsg());
            return response;
        }
        try {
            int insert = this.orderItemDao.insert(orderItem);
            if (insert <= 0 ){
                //日志警告提示
                log.warn("添加失败");
                return DataResponse.error("添加失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
        return DataResponse.success("插入成功",orderItem);
    }

    /**
     * 检查订单商品属性部分信息
     */
    private DataResponse<OrderItem> ckeckOrderItem(OrderItem orderItem) {
        if (null == orderItem.getOrderNo() || orderItem.getOrderNo().trim().compareTo("") == 0){
            return DataResponse.error("订单号不能为空");
        }
        if (null == orderItem.getGoodname() || orderItem.getGoodname().trim().compareTo("") == 0){
            return DataResponse.error("商品名不能为空");
        }
        if (null == orderItem.getGoodbrand() || orderItem.getGoodbrand().trim().compareTo("") == 0){
            return DataResponse.error("商品品牌不能为空");
        }
        if (null == orderItem.getType() || orderItem.getType().trim().compareTo("") == 0){
            return DataResponse.error("商品计价单位不能为空");
        }
        if (null == orderItem.getQuantity()){
            return DataResponse.error("商品数量不能为空");
        }
        if (0 >= orderItem.getQuantity()){
            return DataResponse.error("商品数量不能等于小于0");
        }
        if (null == orderItem.getSalePrice()){
            return DataResponse.error("商品售价不能为空");
        }
        return null;
    }

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse<OrderItem> update(OrderItem orderItem) {
        if (null == orderItem){
            //日志警告提示
            log.warn("订单商品为空");
            return DataResponse.error("订单商品不能为空");
        }
        if (null == orderItem.getId()){
            //日志警告提示
            log.warn("id为空");
            return DataResponse.error("商品ID不能为空");
        }
        //校验数据
        DataResponse<OrderItem> response = ckeckOrderItem(orderItem);
        if (response != null) {
            //日志警告提示
            log.warn("校验不通过 -> {}",response.getMsg());
            return response;
        }
        try {
            //查询是否存在
            OrderItem isorderItem = this.orderItemDao.queryById(orderItem.getId());
            if (null == isorderItem){
                //日志警告提示
                log.warn("订单商品不存在");
                return DataResponse.error("订单商品不存在");
            }
            int update = this.orderItemDao.update(orderItem);
            if (update == 0){
                //日志警告提示
                log.warn("更新失败");
                return DataResponse.error("更新失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
        return this.queryById(orderItem.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public DataResponse deleteById(Integer id) {
        //健壮性判断
        if (null == id){
            //日志警告提示
            log.warn("ID为空");
            return DataResponse.error("ID不能为空");
        }
        try {
            //查询是否存在
            OrderItem orderItem = this.orderItemDao.queryById(id);
            if (null == orderItem){
                //日志警告提示
                log.warn("订单商品不存在");
                return DataResponse.error("订单商品不存在");
            }
            this.orderItemDao.deleteById(id) ;
            return DataResponse.success("删除成功");
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 是否成功
     */
    @Override
    public DataResponse deleteByOrderNo(String orderNo) {
        //健壮性判断
        if (null == orderNo){
            //日志警告提示
            log.warn("ID为空");
            return DataResponse.error("ID不能为空");
        }
        try {
            //查询是否存在
            List<OrderItem> orderItems = this.orderItemDao.queryByOrderNo(orderNo);
            if (null == orderItems || orderItems.size() == 0){
                //日志警告提示
                log.warn("订单商品不存在");
                return DataResponse.error("订单商品不存在");
            }
            this.orderItemDao.deleteByOrderNo(orderNo) ;
            return DataResponse.success("删除成功");
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }
}