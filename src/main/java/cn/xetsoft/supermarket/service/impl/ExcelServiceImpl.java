package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.entity.OrderItem;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.ExcelService;
import cn.xetsoft.supermarket.util.FileUtil;
import cn.xetsoft.supermarket.util.PropertiesUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 使用阿里巴巴提供的EasyExcel读写Excel
 * @since 2020/12/21 19:49
 */

@Slf4j //使用lombok日志的注解
public class ExcelServiceImpl implements ExcelService {

    /**
     * 导出数据库所有内容excel
     * @param goods
     * @param filename
     * @return
     */
    @Override
    public DataResponse writeAllExcel(List<Good> goods, List<Order> orders, List<OrderItem> orderItems, List<User> users, String filename) {
        //向Excel写入数据
        ExcelWriter excelWriter = EasyExcel.write(filename).build();
        if (null != goods) {
            //创建sheet对象
            WriteSheet goodsSheet = EasyExcel.writerSheet("商品信息").head(Good.class).build();
            //写入数据
            excelWriter.write(goods, goodsSheet);
            log.debug("写入商品信息 {}",goods);
        }

        if (null != orders) {
            //创建sheet对象
            WriteSheet orderSheet = EasyExcel.writerSheet("订单信息").head(Order.class).build();
            //写入数据
            excelWriter.write(orders, orderSheet);
            log.debug("写入订单信息 {}",orders);
        }

        if (null != orderItems) {
            //创建sheet对象
            WriteSheet orderItemsSheet = EasyExcel.writerSheet("订单商品信息").head(OrderItem.class).build();
            //写入数据
            excelWriter.write(orderItems, orderItemsSheet);
            log.debug("写入订单商品信息 {}",orderItems);
        }

        if (null != users) {
            //创建sheet对象
            WriteSheet usersSheet = EasyExcel.writerSheet("用户信息").head(User.class).build();
            //写入数据
            excelWriter.write(users, usersSheet);
            log.debug("写入用户信息 {}",users);
        }
        //完成关闭
        excelWriter.finish();
        //返回文件信息
        return DataResponse.success(filename);
    }

    /**
     * 生成模板excel
     * @param filename
     * @return
     */
    @Override
    public DataResponse writeTemplateExcel(String filename) {
        List<Good> goods = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        List<OrderItem> orderItems = new ArrayList<>();
        List<User> users = new ArrayList<>();

        //向Excel写入数据
        ExcelWriter excelWriter = EasyExcel.write(filename).build();
        //创建sheet对象
        WriteSheet goodsSheet = EasyExcel.writerSheet("商品信息").head(Good.class).build();
        //写入数据
        excelWriter.write(goods, goodsSheet);
        //创建sheet对象
        WriteSheet orderSheet = EasyExcel.writerSheet("订单信息").head(Order.class).build();
        //写入数据
        excelWriter.write(orders, orderSheet);


        //创建sheet对象
        WriteSheet orderItemsSheet = EasyExcel.writerSheet("订单商品信息").head(OrderItem.class).build();
        //写入数据
        excelWriter.write(orderItems, orderItemsSheet);

        //创建sheet对象
        WriteSheet usersSheet = EasyExcel.writerSheet("用户信息").head(User.class).build();
        //写入数据
        excelWriter.write(users, usersSheet);
        //完成关闭
        excelWriter.finish();
        log.debug("生成模板信息完成: {}",filename);
        //返回成功消息
        return DataResponse.success();
    }

    /**
     * 导出商品excel
     * @param goods
     * @param filename
     * @return
     */
    @Override
    public DataResponse writeGoodExcel(List<Good> goods, String filename) {
        //向Excel写入数据
        ExcelWriter excelWriter = EasyExcel.write(filename, Good.class).build();
        //创建sheet对象
        WriteSheet writeSheet = EasyExcel.writerSheet("商品信息").build();
        //写入数据
        excelWriter.write(goods,writeSheet);
        //完成关闭
        excelWriter.finish();
        log.debug("生成商品信息完成: {} \n data -> {}",filename,goods);
        //返回文件信息
        return DataResponse.success(filename);
    }

    /**
     * 导出订单excel 包含订单商品
     */
    @Override
    public DataResponse writeOrderExcel(List<Order> orders,List<OrderItem> orderItems,String filename) {
        //向Excel写入数据
        ExcelWriter excelWriter = EasyExcel.write(filename, Order.class).build();
        //创建sheet对象
        WriteSheet orderSheet = EasyExcel.writerSheet("订单信息").build();
        //写入数据
        excelWriter.write(orders,orderSheet);
        //创建sheet对象
        WriteSheet orderItemSheet = EasyExcel.writerSheet("订单商品信息").build();
        //写入数据
        excelWriter.write(orderItems,orderItemSheet);
        //完成关闭
        excelWriter.finish();
        log.debug("生成订单信息完成: {} \n data -> {} , {}",filename,orders,orderItems);
        //返回文件信息
        return DataResponse.success(filename);
    }

    /**
     * 导出用户excel
     * @param users
     * @return
     */
    @Override
    public DataResponse writeUserExcel(List<User> users,String filename) {
        //向Excel写入数据
        ExcelWriter excelWriter = EasyExcel.write(filename, User.class).build();
        //创建sheet对象
        WriteSheet writeSheet = EasyExcel.writerSheet("用户信息").build();
        //写入数据
        excelWriter.write(users,writeSheet);
        //完成关闭
        excelWriter.finish();
        log.debug("生成用户信息完成: {} \n data -> {}",filename,users);
        //返回文件信息
        return DataResponse.success(filename);
    }

    /**
     * 读取初始化商品数据表
     * @param fileName
     * @return
     */
    @Override
    public List<Good> readGoodExcel(String fileName) {
        List<Good> goods = new ArrayList<>();
        ExcelReader excelReader = EasyExcel.read(fileName, Good.class, new AnalysisEventListener<Good>() {
            /**
             * 解析一行调用一次
             * @param good
             * @param analysisContext
             */
            @Override
            public void invoke(Good good, AnalysisContext analysisContext) {
                log.debug("读取商品信息: {} ",good);
                goods.add(good);
            }

            /**
             * 全部解析完调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.debug("解析完成");
            }
        }).build();

        //创建读取表对象
        ReadSheet readSheet = EasyExcel.readSheet("商品信息").build();
        //读取工作表信息
        excelReader.read(readSheet);
        //完成
        excelReader.finish();
        return goods;
    }

    /**
     * 读取初始化订单数据表
     * @param fileName
     * @return
     */
    @Override
    public List<Order> readOrderExcel(String fileName) {
        List<Order> orders = new ArrayList<>();
        ExcelReader excelReader = EasyExcel.read(fileName, Order.class, new AnalysisEventListener<Order>() {
            /**
             * 解析一行调用一次
             * @param order
             * @param analysisContext
             */
            @Override
            public void invoke(Order order, AnalysisContext analysisContext) {
                log.debug("读取订单信息: {} ",order);
                orders.add(order);
            }

            /**
             * 全部解析完调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.debug("解析完成");
            }
        }).build();

        //创建读取表对象
        ReadSheet readSheet = EasyExcel.readSheet("订单信息").build();
        //读取工作表信息
        excelReader.read(readSheet);
        //完成
        excelReader.finish();
        //返回读取信息
        return orders;
    }

    /**
     * 读取初始化订单商品数据表
     * @param fileName
     * @return
     */
    @Override
    public List<OrderItem> readOrderItemExcel(String fileName) {
        List<OrderItem> orderItems = new ArrayList<>();
        ExcelReader excelReader = EasyExcel.read(fileName, OrderItem.class, new AnalysisEventListener<OrderItem>() {
            /**
             * 解析一行调用一次
             * @param orderItem
             * @param analysisContext
             */
            @Override
            public void invoke(OrderItem orderItem, AnalysisContext analysisContext) {
                log.debug("读取订单商品信息: {} ",orderItem);
                orderItems.add(orderItem);
            }

            /**
             * 全部解析完调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.debug("解析完成");
            }
        }).build();

        //创建读取表对象
        ReadSheet readSheet = EasyExcel.readSheet("订单商品信息").build();
        //读取工作表信息
        excelReader.read(readSheet);
        //完成
        excelReader.finish();
        //返回读取信息
        return orderItems;
    }

    /**
     * 读取初始化用户数据表
     * @param fileName
     * @return
     */
    @Override
    public List<User> readUserExcel(String fileName) {
        List<User> users = new ArrayList<>();
        ExcelReader excelReader = EasyExcel.read(fileName, User.class, new AnalysisEventListener<User>() {
            /**
             * 解析一行调用一次
             * @param user
             * @param analysisContext
             */
            @Override
            public void invoke(User user, AnalysisContext analysisContext) {
                log.debug("读取用户信息: {} ",user);
                users.add(user);
            }

            /**
             * 全部解析完调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.debug("解析完成");
            }
        }).build();

        //创建读取表对象
        ReadSheet readSheet = EasyExcel.readSheet("用户信息").build();
        //读取工作表信息
        excelReader.read(readSheet);
        //完成
        excelReader.finish();
        //返回读取信息
        return users;
    }
}
