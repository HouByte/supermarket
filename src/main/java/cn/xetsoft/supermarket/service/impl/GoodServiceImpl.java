package cn.xetsoft.supermarket.service.impl;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.dao.GoodDao;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.GoodService;
import cn.xetsoft.supermarket.util.RegexUtils;
import lombok.extern.slf4j.Slf4j;


import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/19 18:30
 * @version 1.0
 * @Description (Good)表服务实现类
 */
@Slf4j //使用lombok日志的注解
public class GoodServiceImpl implements GoodService {
    //数据库操作类
    private GoodDao goodDao;

    //需要外部提供实现类操作
    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param gid 主键
     * @return 实例对象
     */
    @Override
    public DataResponse<Good> queryById(Integer gid) {
        //健壮性
        if (null == gid){
            //日志警告提示
            log.warn("gid -> {}",gid);
            //通过代理类传递错误消息
            return DataResponse.error("商品gid不能为空");
        }
        try {
            //通过调用dao数据库操作实现功能
            Good good = this.goodDao.queryById(gid);
            if (null == good){
                //日志警告提示
                log.warn("商品不存在");
                //通过代理类传递错误消息
                return DataResponse.error("查询商品不存在");
            }
            //通过代理类传递成功消息
            return DataResponse.success(good);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 通过商品名/品牌名称模糊查询多条数据
     * @param keyword 关键字
     * @return 实例对象
     */
    @Override
    public DataResponse<List<Good>> queryByGoodnameAndGoodbrand(String keyword) {
        //健壮性
        if (null == keyword){
            //日志警告提示
            log.warn("keyword -> {}",keyword);
            //通过代理类传递错误消息
            return DataResponse.error("关键字不能为空");
        }
        try {
            //通过调用dao数据库操作实现功能
            List<Good> goods = this.goodDao.queryByLikeGoodnameAndGoodbrand(keyword);
            //健壮性检查列表是否存在消息
            if (goods.size() == 0){
                //日志警告提示
                log.warn("商品列表为空");
                //通过代理类传递错误消息
                return DataResponse.error("查询商品列表为空");
            }
            //通过代理类传递成功消息
            return DataResponse.success(goods);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 通过商品名查询单条数据
     *
     * @param goodname 关键字
     * @return 实例对象
     */
    @Override
    public DataResponse<Good> queryByGoodname(String goodname) {
        //健壮性
        if (null == goodname){
            //日志警告提示
            log.warn("goodname -> {}",goodname);
            //通过代理类传递错误消息
            return DataResponse.error("商品名称不能为空");
        }
        try {
            //通过调用dao数据库操作实现功能
            Good good = this.goodDao.queryByGoodname(goodname);
            //健壮性检查列表是否存在消息
            if (good == null){
                //日志警告提示
                log.warn("商品不存在");
                //通过代理类传递错误消息
                return DataResponse.error("查询商品不存在");
            }
            //通过代理类传递成功消息
            return DataResponse.success(good);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }

    /**
     * 查询多条数据
     * @return 对象列表
     */
    @Override
    public DataResponse<List<Good>> queryAll() {
        try {
            //通过调用dao数据库操作实现功能
            List<Good> goods = this.goodDao.queryAll();
            //健壮性检查列表是否存在消息
            if (goods.size() == 0){
                //日志警告提示
                log.warn("商品列表为空");
                //通过代理类传递错误消息
                return DataResponse.error("查询商品列表为空");
            }
            //通过代理类传递成功消息
            return DataResponse.success(goods);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
    }


    /**
     * 新增数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse<Good> insert(Good good) {
        //健壮性判断
        if (null == good){
            //日志警告提示
            log.warn("添加商品不能为空");
            return DataResponse.error("添加品不能为空");
        }
        //校验非主信息
        DataResponse goodNullable = isGoodNullable(good);
        if (null != goodNullable){
            //日志警告提示
            log.warn("校验数据失败 -> {}",goodNullable.getMsg());
            return goodNullable;
        }

        try {
            Good byGoodname = this.goodDao.queryByGoodname(good.getGoodname());
            if (null != byGoodname){
                //日志警告提示
                log.warn("商品名称存在重复");
                return DataResponse.error("商品名不能重复");
            }
            //通过调用dao数据库操作实现功能
            this.goodDao.insert(good);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
        //通过代理类传递成功消息
        return DataResponse.success("添加成功",good);
    }

    /**
     * 修改数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    @Override
    public DataResponse<Good> update(Good good) {
        //健壮性判断
        if (null == good){
            //日志警告提示
            log.warn("");
            return DataResponse.error("商品不能为空");
        }

        //校验主键
        if (good.getGid() == null){
            //日志警告提示
            log.warn("gid -> {}",good.getGid());
            return DataResponse.error("商品编号不能为空");
        }
        //校验非主信息
        DataResponse goodNullable = isGoodNullable(good);
        if (null != goodNullable){
            //日志警告提示
            log.warn("校验数据失败 -> {}",goodNullable.getMsg());
            return goodNullable;
        }

        try {
            //查询商品是否存在
            Good isgood = this.goodDao.queryById(good.getGid());
            if (null == isgood){
                //日志警告提示
                log.warn("");
                return DataResponse.error("商品不存在");
            }
            //通过调用dao数据库操作实现功能
            int update = this.goodDao.update(good);
            if (update == 0){
                //日志警告提示
                log.warn("");
                return DataResponse.error("更新失败");
            }
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
        //查询更新商品返回
        return this.queryById(good.getGid());
    }

    /**
     * 非主键健壮性判断
     * 检测数据是否存在空值
     * @param good
     * @return
     */
    private DataResponse<Object> isGoodNullable(Good good) {
        /**
         * 判断是否为null
         * 字符串判断去空格之后是否为空
         */
        if (good.getGoodname() == null || good.getGoodname().trim().compareTo("")==0){
            return DataResponse.error("商品名不能为空");//通过代理类传递失败消息
        }
        if (good.getGoodbrand() == null || good.getGoodbrand().trim().compareTo("")==0){
            return DataResponse.error("商品品牌不能为空");//通过代理类传递失败消息
        }
        if (good.getInprice() == null){
            return DataResponse.error("商品进价不能为空");//通过代理类传递失败消息
        }
        if (good.getSaleprice() == null ){
            return DataResponse.error("商品售价不能为空");//通过代理类传递失败消息
        }
        if (good.getSaleprice().doubleValue() <= 0 ){
            return DataResponse.error("商品售价不能为0或小于0");//通过代理类传递失败消息
        }
        if (good.getType() == null || good.getType().trim().compareTo("")==0){
            return DataResponse.error("商品计价单位不能为空");//通过代理类传递失败消息
        }
        if (good.getStock() == null ){
            return DataResponse.error("库存不能为空");//通过代理类传递失败消息
        }
        if (good.getIndate() == null ){
            return DataResponse.error("过期时间不能为空");//通过代理类传递失败消息
        }
        if (good.getStock() < 0 ){
            return DataResponse.error("商品售价不能小于0");//通过代理类传递失败消息
        }
        if (good.getAgent() == null || good.getAgent().trim().compareTo("")==0){
            return DataResponse.error("经手人不能为空");//通过代理类传递失败消息
        }

        if (good.getSaleprice().doubleValue() < good.getInprice().doubleValue() ){
            return DataResponse.error("商品售价不能少于进价");//通过代理类传递失败消息
        }
        return null;
    }

    /**
     * 通过主键删除数据
     *
     * @param gid 主键
     * @return 是否成功
     */
    @Override
    public DataResponse deleteById(Integer gid) {
        //健壮性判断
        if(null == gid){
            //日志警告提示
            log.warn("商品id为空");
            return DataResponse.error("id不能为空");
        }
        try {
            //查询商品是否存在
            Good good = this.goodDao.queryById(gid);
            if (null == good){
                //日志警告提示
                log.warn("商品不存在");
                return DataResponse.error("商品不存在");
            }
            //通过调用dao数据库操作实现功能
            this.goodDao.deleteById(gid);
        } catch (SQLException throwables) {
            //日志警告提示
            log.warn("异常 -> {}",throwables.getMessage());
            //通过代理类传递异常消息
            return DataResponse.errorException(throwables.getMessage());
        }
        //通过代理类传递成功消息
        return DataResponse.success("删除成功");
    }
}