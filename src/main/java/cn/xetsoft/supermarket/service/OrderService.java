package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.Cart;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.vo.OrderVo;

import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (Order)表服务接口
 */
public interface OrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    DataResponse<Order> queryById(Integer id);

    /**
     * 通过订单号查询单条数据
     *
     * @param orderNo 订单号
     * @return 实例对象
     */
    DataResponse<Order> queryByOrderNo(String orderNo);

    /**
     * 通过订单号模糊查询多条数据
     *
     * @param orderNo 订单号
     * @return 实例对象
     */
    DataResponse<List<Order>> queryByOrderNoLike(String orderNo);
    /**
     * 通过订单号查询单条数据
     *
     * @param date 时间
     * @return 实例对象
     */
    DataResponse<OrderVo> queryByDate(String date);

    /**
     * 通过订单号查询单条数据
     * @return 实例对象
     */
    DataResponse<OrderVo> queryByToday();
    /**
     * 通过订单号查询单条数据
     *
     * @return 实例对象
     */
    DataResponse<OrderVo> queryByNowMonth();
    /**
     * 查询所有
     * @return 实例对象
     */
    DataResponse<OrderVo> queryAll();

    /**
     * 新增数据
     *
     * @param carts 购物车对象
     * @return 实例对象
     */
    DataResponse<OrderVo> insert(List<Cart> carts);

    /**
     * 新增数据
     * 初始化使用，其他不建议使用
     * @param order 订单
     * @return 实例对象
     */
    DataResponse<OrderVo> insert(Order order);


    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    DataResponse<OrderVo> update(Order order);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    DataResponse deleteById(Integer id);
}