package cn.xetsoft.supermarket.service;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.OrderItem;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (OrderItem)表服务接口
 */
public interface OrderItemService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    DataResponse<OrderItem> queryById(Integer id);

    /**
     * 查询所有
     * @return 实例对象
     */
    DataResponse<List<OrderItem>> queryAll();

    /**
     * 通过订单号查询
     * @return 实例对象
     */
    DataResponse<List<OrderItem>> queryByOrderNo(String orderNo);

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    DataResponse<OrderItem> insert(OrderItem orderItem);

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    DataResponse<OrderItem> update(OrderItem orderItem);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    DataResponse deleteById(Integer id);

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 是否成功
     */
    DataResponse deleteByOrderNo(String orderNo);

}