package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.dao.OrderDao;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.util.JDBCUtils;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单数据库操作
 * @since 2020/12/20 21:17
 */
@Slf4j //使用lombok日志的注解
public class OrderDaoImpl implements OrderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Order queryById(Integer id) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  `order` where id=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setInt(1,id);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",id);

        //执行
        ResultSet rs = ptmt.executeQuery();
        Order order = null;
        while(rs.next()){
            // 数据库结果转换Order的对象
            order = resultDtoOrder(rs);

        }

        return order;
    }

    /**
     * 通过OrderNo查询单条数据
     *
     * @param orderNo 主键
     * @return 实例对象
     */
    @Override
    public Order queryByorderNo(String orderNo) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  `order` where order_no =?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setString(1,orderNo);
        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);
        //执行
        ResultSet rs = ptmt.executeQuery();
        Order order = null;
        while(rs.next()){
            // 数据库结果转换Order的对象
            order = resultDtoOrder(rs);

        }

        return order;
    }

    /**
     * 通过OrderNo模糊查询多条数据
     *
     * @param orderNo 订单号
     * @return 实例对象
     */
    @Override
    public List<Order> queryByOrderNoLike(String orderNo) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  `order` where order_no  like \"%"+orderNo+"%\"";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);
        //执行
        ResultSet rs = ptmt.executeQuery();
        //获取数据
        List<Order> orders = new ArrayList<Order>();
        while(rs.next()){
            // 数据库结果转换Orderd对象
            Order order = resultDtoOrder(rs);
            //加入列表
            orders.add(order);
        }

        return orders;
    }

    /**
     * 通过OrderNo查询单条数据
     *
     * @param date 日期
     * @return 实例对象
     */
    @Override
    public List<Order> queryByDate(String date) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  `order` WHERE  sale_time like \"%"+date+"%\"";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",date);
        //执行
        ResultSet rs = ptmt.executeQuery();
        //获取数据
        List<Order> orders = new ArrayList<Order>();
        while(rs.next()){
            // 数据库结果转换Orderd对象
            Order order = resultDtoOrder(rs);
            //加入列表
            orders.add(order);
        }

        return orders;
    }

    /**
     * 通过OrderNo查询单条数据
     *
     * @param orderNo 主键
     * @return 实例对象
     */
    @Override
    public boolean checkOrderNo(String orderNo) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select COUNT(0) AS count from  `order` where order_no =?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setString(1,orderNo);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);

        //执行
        ResultSet rs = ptmt.executeQuery();
        Order order = null;
        while(rs.next()){
            Integer count = rs.getInt("count");
            if (count > 0){
                return true;
            }
        }
        return false;
    }

    /**
     * 数据库结果转换order的对象
     * 获取所有属性，设置到类中
     */
    private Order resultDtoOrder(ResultSet rs) throws SQLException {
        Order order = new Order();
        /**
         * 获取所有属性，设置到类中
         */
        //id, order_no, `desc`, total_price, profit, agent, saletime
        //从结果中设置id
        order.setId(rs.getInt("id"));
        //从结果中设置订单号
        order.setOrderNo(rs.getString("order_no"));
        //从结果中设置订单详情
        order.setDesc(rs.getString("desc"));
        //从结果中设置总价
        order.setTotalPrice(rs.getBigDecimal("total_price"));
        //从结果中设置设置利润
        order.setProfit(rs.getBigDecimal("profit"));
        //从结果中设置经办人
        order.setAgent(rs.getString("agent"));
        //从结果中设置销售时间
        order.setSaleTime(rs.getDate("sale_time"));
        order.setUpdateTime(rs.getDate("update_time"));
        return order;
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @return 对象列表
     */
    @Override
    public List<Order> queryAll() throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  `order`";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //打印debug日志
        log.debug("run sql : {}",sql);

        //执行
        ResultSet rs = ptmt.executeQuery();
        List<Order> orders = new ArrayList<Order>();
        while(rs.next()){
            // 数据库结果转换Orderd对象
            Order order = resultDtoOrder(rs);
            //加入列表
            orders.add(order);
        }

        return orders;
    }

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    @Override
    public void insert(Order order) throws SQLException {
        //获取连接 通过单例模式 懒汉获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql
        String sql = "INSERT INTO `order`( order_no, `desc`, total_price, profit, agent, sale_time,update_time)"
                +"values("+"?,?,?,?,?,CURRENT_DATE(),CURRENT_DATE())";

        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行

        //传参
        //id主键属性数据库自增 插入不操作
        // order部分与更新方法共同方法
        orderDtoParam(order, ptmt);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",order);

        //执行并返回结果
        ptmt.execute();

    }

    /**
     * 公用参数设置
     */
    private void orderDtoParam(Order order, PreparedStatement ptmt) throws SQLException {
        ptmt.setString(1, order.getOrderNo());
        ptmt.setString(2, order.getDesc());
        ptmt.setBigDecimal(3, order.getTotalPrice());
        ptmt.setBigDecimal(4, order.getProfit());
        ptmt.setString(5, order.getAgent());
    }

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Order order) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "UPDATE `order`" +
                " set order_no=?, `desc`=?, total_price=?, profit=?, agent=?, update_time=CURRENT_DATE()"+
                " where id=?";
        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行

        //传参
        // order部分与更新方法共同方法
        orderDtoParam(order, ptmt);
        //设置主键id
        ptmt.setInt(6,order.getId());

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",order);

        //执行
        return ptmt.executeUpdate();
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    @Override
    public void deleteById(Integer id) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "delete from `order` where id=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //传参
        ptmt.setInt(1, id);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",id);
        //执行
        ptmt.execute();

    }

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 影响行数
     */
    @Override
    public void deleteByOrderNo(String orderNo) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "delete from `order` where order_no=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //传参
        ptmt.setString(1, orderNo);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);

        //执行
        ptmt.execute();
    }
}
