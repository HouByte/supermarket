package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.dao.OrderItemDao;
import cn.xetsoft.supermarket.entity.OrderItem;
import cn.xetsoft.supermarket.util.JDBCUtils;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单商品数据库操作
 * @since 2020/12/20 21:18
 */
@Slf4j //使用lombok日志的注解
public class OrderItemDaoImpl implements OrderItemDao {
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public OrderItem queryById(Integer id) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  order_item where id=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setInt(1,id);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",id);

        //执行
        ResultSet rs = ptmt.executeQuery();
        OrderItem orderItem = null;
        while(rs.next()){
            // 数据库结果转换orderItems的对象
            orderItem = rusultDtoOrderItem(rs);
        }

        return orderItem;
    }

    /**
     * 查询所有订单商品
     *
     * @return 对象列表
     */
    @Override
    public List<OrderItem> queryAll() throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  order_item";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //打印debug日志
        log.debug("run sql : {}",sql);
        //执行
        ResultSet rs = ptmt.executeQuery();
        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        while(rs.next()){

            // 数据库结果转换orderItems的对象
            OrderItem orderItem = rusultDtoOrderItem(rs);
            //加入列表
            orderItems.add(orderItem);
        }

        return orderItems;
    }

    /**
     * 通过订单号作为筛选条件查询
     *
     * @return 对象列表
     * @param orderNo 订单号
     */
    @Override
    public List<OrderItem> queryByOrderNo(String orderNo) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  order_item where order_no=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setString(1,orderNo);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);

        //执行
        ResultSet rs = ptmt.executeQuery();
        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        while(rs.next()){

            // 数据库结果转换orderItems的对象
            OrderItem orderItem = rusultDtoOrderItem(rs);
            //加入列表
            orderItems.add(orderItem);
        }

        return orderItems;
    }

    /**
     * 数据库结果转换orderItems的对象
     * 获取所有属性，设置到类中
     */
    private OrderItem rusultDtoOrderItem(ResultSet rs) throws SQLException {
        OrderItem orderItem = new OrderItem();
        /**
         * 获取所有属性，设置到类中
         */
        //id,order_no,goodname,goodbrand,sale_price,type,quantity,create_time,update_time
        //从结果中设置id
        orderItem.setId(rs.getInt("id"));
        //从结果中设置设置订单号
        orderItem.setOrderNo(rs.getString("order_no"));
        //从结果中设置商品名
        orderItem.setGoodname(rs.getString("goodname"));
        //从结果中设置订单品牌
        orderItem.setGoodbrand(rs.getString("goodbrand"));
        //从结果中设置售价
        orderItem.setSalePrice(rs.getBigDecimal("sale_price"));
        //从结果中设置类型
        orderItem.setType(rs.getString("type"));
        //从结果中设置数量
        orderItem.setQuantity(rs.getInt("quantity"));
        //从结果中设置创建时间
        orderItem.setCreateTime(rs.getDate("create_time"));
        //从结果中设置更新时间
        orderItem.setUpdateTime(rs.getDate("update_time"));
        return orderItem;
    }

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(OrderItem orderItem) throws SQLException {
        //获取连接 通过单例模式 懒汉获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql
        String sql = "INSERT INTO order_item(order_no,goodname,goodbrand,sale_price,`type`,quantity,create_time,update_time)"
                +"values("+"?,?,?,?,?,?,CURRENT_DATE(),CURRENT_DATE())";
        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行

        //传参
        //订单号禁止修改只有插入时单独设置
        ptmt.setString(1,orderItem.getOrderNo());
        //部分数据转换PreparedStatement参数插入sql语句
        orderItemDtoParam(orderItem, ptmt,2);


        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderItem);
        //执行并返回结果
        return ptmt.executeUpdate();

    }

    /**
     * 部分数据转换PreparedStatement参数插入sql语句
     * index 第几个下标开始
     */
    private void orderItemDtoParam(OrderItem orderItem, PreparedStatement ptmt,int index) throws SQLException {
        ptmt.setString(index++, orderItem.getGoodname());
        ptmt.setString(index++, orderItem.getGoodbrand());
        ptmt.setBigDecimal(index++, orderItem.getSalePrice());
        ptmt.setString(index++, orderItem.getType());
        ptmt.setInt(index++, orderItem.getQuantity());
    }

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    @Override
    public int update(OrderItem orderItem) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "UPDATE order_item" +
                " set goodname=?,goodbrand=?,sale_price=?,`type`=?,quantity=?,update_time = CURRENT_DATE()"+
                " where id=?";
        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行

        //传参
        //部分数据转换PreparedStatement参数插入语句
        orderItemDtoParam(orderItem, ptmt,1);
        //设置主键
        ptmt.setInt(6,orderItem.getId());

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderItem);
        //执行
        return ptmt.executeUpdate();
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    @Override
    public int deleteById(Integer id) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "delete from order_item where id=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //传参
        ptmt.setInt(1, id);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",id);

        //执行
        return ptmt.executeUpdate();
    }

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 是否成功
     */
    @Override
    public int deleteByOrderNo(String orderNo) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "delete from order_item where order_no=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //传参
        ptmt.setString(1, orderNo);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",orderNo);

        //执行
        return ptmt.executeUpdate();
    }
}
