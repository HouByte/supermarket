package cn.xetsoft.supermarket.dao.impl;

import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.dao.GoodDao;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.util.JDBCUtils;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 侯国强
 * @version 1.0
 * @Description  (Good)表数据库访问层实现
 * @since 2020/12/20 15:12
 */

@Slf4j //使用lombok日志的注解
public class GoodDaoImpl implements GoodDao {

    /**
     * 通过ID查询单条数据
     * @param gid 主键
     * @return 实例对象
     */
    @Override
    public Good queryById(Integer gid) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  good where gid=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setInt(1, gid);
        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",gid);
        //执行
        ResultSet rs = ptmt.executeQuery();
        Good good = null;
        while(rs.next()){
            //获取所有属性，设置到类中
            good = resultDtoGood(rs);
        }
        //如果出现重复的数据，之后获取最后一个数据
        return good;
    }

    /**
     * 通过商品名模糊查询单条数据
     *
     * @param goodname 商品名称
     * @return 实例对象
     */
    @Override
    public Good queryByGoodname(String goodname) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "select * from  good where goodname=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //传参
        ptmt.setString(1, goodname);
        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",goodname);
        //执行
        ResultSet rs = ptmt.executeQuery();
        Good good = null;
        while(rs.next()){
            //获取所有属性，设置到类中
            good = resultDtoGood(rs);
        }
        //如果出现重复的数据，之后获取最后一个数据
        return good;
    }

    /**
     * 通过商品名模糊查询单条数据
     * @param keyword 关键字
     * @return 实例对象
     */
    @Override
    public List<Good> queryByLikeGoodnameAndGoodbrand(String keyword) throws SQLException {
        return getGoods(keyword);
    }

    /**
     * 通过实体作为筛选条件查询
     * @return 对象列表
     */
    @Override
    public List<Good> queryAll() throws SQLException {
        return getGoods(null);
    }

    /**
     * 查询商品
     * @param keyword
     * @return
     * @throws SQLException
     */
    private List<Good> getGoods(String keyword) throws SQLException {
        //获取连接 使用单例
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格 1=1 始终成立，为了动态拼接sql方便
//        String sql = "SELECT * FROM  good WHERE goodname like \"%"+keyword+"%\" or goodbrand like \"%"+keyword+"%\" ";
        String sql = "SELECT * FROM  good where 1=1";
        if (keyword !=null){
            sql += " AND goodname like \"%"+keyword+"%\" or goodbrand like \"%"+keyword+"%\" ";
            //打印debug日志
            log.debug("run sql : {}",sql);
            log.debug("param -> {}",keyword);
        } else {
            //打印debug日志
            log.debug("run sql : {}",sql);
        }


        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);
        //执行
        ResultSet rs = ptmt.executeQuery();
        //创建列表
        List<Good> goods = new ArrayList<Good>();
        while(rs.next()){

            //获取所有属性，设置到类中
            Good good = resultDtoGood(rs);
            //添加商品到列表中
            goods.add(good);
        }
        //如果出现重复的数据，之后获取最后一个数据
        return goods;
    }

    /**
     * 结果转换到Good对象
     */
    private Good resultDtoGood(ResultSet rs) throws SQLException {
        Good good = new Good();
        /**
         * 获取所有属性，设置到类中
         * gid, goodname, goodbrand, intime, saletime, inprice, saleprice, type, stock, agent, create_time, update_time
         */
        //从结果中设置id
        good.setGid(rs.getInt("gid"));
        //从结果中设置商品名
        good.setGoodname(rs.getString("goodname"));
        //从结果中设置商品品牌
        good.setGoodbrand(rs.getString("goodbrand"));
        //从结果中设置进货时间
        good.setIntime(rs.getTime("intime"));
        //从结果中设置销售时间
        good.setSaletime(rs.getTime("saletime"));
        //从结果中设置进货价
        good.setInprice(rs.getBigDecimal("inprice"));
        //从结果中设置售价
        good.setSaleprice(rs.getBigDecimal("saleprice"));
        //从结果中设置计价单位
        good.setType(rs.getString("type"));
        //过期时间
        good.setIndate(rs.getDate("indate"));
        //从结果中设置库存
        good.setStock(rs.getInt("stock"));
        //从结果中设置经办人
        good.setAgent(rs.getString("agent"));
        //从结果中设置创建时间
        good.setCreateTime(rs.getDate("create_time"));
        //从结果中设置更新时间
        good.setUpdateTime(rs.getDate("update_time"));
        return good;
    }


    /**
     * 新增数据
     * @param good 实例对象
     * @return 影响行数
     */
    @Override
    public int insert(Good good) throws SQLException {
        //获取连接 通过单例模式 懒汉获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql
        String sql = "INSERT INTO good( goodname, goodbrand, intime, saletime, inprice, saleprice, `type`, indate,stock, agent, create_time, update_time)"
                +"values("+"?,?,CURRENT_DATE(),CURRENT_DATE(),?,?,?,?,?,? ,CURRENT_DATE(),CURRENT_DATE())";
        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行
        //gid, goodname, goodbrand, intime, saletime, inprice, saleprice, type, stock, agent, create_time, update_time
        /*
         * 传参
         */
        goodDtoParam(good, ptmt);
        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",good);
        //执行并返回结果
       return ptmt.executeUpdate();
    }

    /**
     * 修改数据
     * @param good 实例对象
     * @return 影响行数
     */
    @Override
    public int update(Good good) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "UPDATE good" +
                " set goodname=?, goodbrand=?, inprice=?, saleprice=?, `type`=?,indate=?, stock=?, agent=?,update_time = CURRENT_DATE()"+
                " where gid=?";
        //预编译
        PreparedStatement ptmt = conn.prepareStatement(sql); //预编译SQL，减少sql执行
        //gid, goodname, goodbrand, intime, saletime, inprice, saleprice, type, stock, agent, create_time, update_time
        /*
         * 传参
         */
        //通用参数封装设置
        goodDtoParam(good, ptmt);
        //设置特有条件
        ptmt.setInt(9,good.getGid());

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",good);
        //执行并返回结果
        return ptmt.executeUpdate();
    }


    private void goodDtoParam(Good good, PreparedStatement ptmt) throws SQLException {
        //设置商品名
        ptmt.setString(1, good.getGoodname());
        //设置商品品牌
        ptmt.setString(2, good.getGoodbrand());
        //设置商品进价
        ptmt.setBigDecimal(3, good.getInprice());
        //设置商品售价
        ptmt.setBigDecimal(4, good.getSaleprice());
        //设置商品计价单位
        ptmt.setString(5, good.getType());
        //设置过期日期
        ptmt.setDate(6, new java.sql.Date(good.getIndate().getTime()));
        //商品库存
        ptmt.setInt(7, good.getStock());
        //设置经办人
        ptmt.setString(8, good.getAgent());
    }

    /**
     * 通过主键删除数据
     * @param gid 主键
     * @return 影响行数
     */
    @Override
    public int deleteById(Integer gid) throws SQLException {
        //获取连接
        Connection conn = JDBCUtils.getInstanceConnection();
        //sql, 每行加空格
        String sql = "delete from good where gid=?";
        //预编译SQL，减少sql执行
        PreparedStatement ptmt = conn.prepareStatement(sql);

        //传参
        ptmt.setInt(1, gid);

        //打印debug日志
        log.debug("run sql : {}",sql);
        log.debug("param -> {}",gid);
        //执行
        return ptmt.executeUpdate();
    }
}
