package cn.xetsoft.supermarket.dao;

import java.sql.SQLException;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 数据库功能基本操作
 * @since 2020/12/23 15:30
 */
public interface DbDao {

    /**
     * 使用数据库
     */
    int useDatabase(String dbName) throws SQLException;

    /**
     * 创建数据库
     */
    int createDatabase(String dbName) throws SQLException;

    /**
     * 创建数据表
     * 需要提供数据表创建sql
     */
    int createTable(String sql) throws SQLException;

    /**
     * 删除数据库
     */
    int deleteDatabase(String dbName) throws SQLException;

    /**
     * 判断数据库是否存在
     */
    boolean existDatabase(String dbName) throws SQLException;

    /**
     * 判断数据表是否存在
     */
    boolean existTable(String dbName,String tbName) throws SQLException;
}
