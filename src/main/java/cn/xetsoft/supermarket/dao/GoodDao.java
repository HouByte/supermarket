package cn.xetsoft.supermarket.dao;

import cn.xetsoft.supermarket.entity.Good;

import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/19 20:22
 * @version 1.0
 * @Description (Good)表数据库访问层
 */
public interface GoodDao {

    /**
     * 通过ID查询单条数据
     * @param gid 主键
     * @return 实例对象
     */
    Good queryById(Integer gid) throws SQLException;

    /**
     * 通过商品名模糊查询单条数据
     * @param goodname 商品名称
     * @return 实例对象
     */
    Good queryByGoodname(String goodname) throws SQLException;


    /**
     * 通过商品名模糊查询多条数据
     * @param keyword 关键字
     * @return 实例对象
     */
    List<Good> queryByLikeGoodnameAndGoodbrand(String keyword) throws SQLException;

    /**
     * 通过实体作为筛选条件查询
     * @return 对象列表
     */
    List<Good> queryAll() throws SQLException;

    /**
     * 新增数据
     * @param good 实例对象
     * @return 影响行数
     */
    int insert(Good good) throws SQLException;

    /**
     * 修改数据
     * @param good 实例对象
     * @return 影响行数
     */
    int update(Good good) throws SQLException;

    /**
     * 通过主键删除数据
     * @param gid 主键
     * @return 影响行数
     */
    int deleteById(Integer gid) throws SQLException;

}