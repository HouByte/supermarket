package cn.xetsoft.supermarket.dao;

import cn.xetsoft.supermarket.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Vincent Vic
 * @since 2020/12/19 20:22
 * @version 1.0
 * @Description (User)表数据库访问层
 */
public interface UserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id) throws SQLException;

    /**
     * 通过ID查询单条数据
     *
     * @param username 主键
     * @return 实例对象
     */
    User queryByUserName(String username) throws SQLException;


    /**
     * 查询全部
     * @return 对象列表
     */
    List<User> queryAll() throws SQLException;

    /**
     * 登入操作
     * @param username 用户名
     * @param password 密码
     * @return 成功返回用户对象，失败为空
     */
    User login(String username,String password) throws SQLException;

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insert(User user) throws SQLException;

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int update(User user) throws SQLException;

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id) throws SQLException;

}