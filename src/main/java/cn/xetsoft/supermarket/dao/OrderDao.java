package cn.xetsoft.supermarket.dao;

import cn.xetsoft.supermarket.entity.Order;

import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (Order)表数据库访问层
 */
public interface OrderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Order queryById(Integer id) throws SQLException;

    /**
     * 通过OrderNo查询单条数据
     *
     * @param orderNo 订单号
     * @return 实例对象
     */
    Order queryByorderNo(String orderNo) throws SQLException;


    /**
     * 通过OrderNo模糊查询多条数据
     *
     * @param orderNo 订单号
     * @return 实例对象
     */
    public List<Order> queryByOrderNoLike(String orderNo) throws SQLException;

    /**
     * 通过OrderNo查询单条数据
     *
     * @param date 日期
     * @return 实例对象
     */
    List<Order> queryByDate(String date) throws SQLException;

    /**
     * 通过OrderNo查询单条数据
     *
     * @param orderNo 主键
     * @return 实例对象
     */
    boolean checkOrderNo(String orderNo) throws SQLException;

    /**
     * 通过实体作为筛选条件查询
     *
     * @return 对象列表
     */
    List<Order> queryAll() throws SQLException;

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    void insert(Order order) throws SQLException;

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int update(Order order) throws SQLException;

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    void deleteById(Integer id) throws SQLException;

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 影响行数
     */
    void deleteByOrderNo(String orderNo) throws SQLException;

}