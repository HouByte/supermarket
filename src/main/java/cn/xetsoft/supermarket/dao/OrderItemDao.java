package cn.xetsoft.supermarket.dao;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.entity.OrderItem;

import java.sql.SQLException;
import java.util.List;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (OrderItem)表数据库访问层
 */
public interface OrderItemDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    OrderItem queryById(Integer id) throws SQLException;

    /**
     * 查询所有订单商品
     *
     * @return 对象列表
     */
    List<OrderItem> queryAll() throws SQLException;

    /**
     * 通过订单号作为筛选条件查询
     *
     * @return 对象列表
     * @param orderNo 订单号
     */
    List<OrderItem> queryByOrderNo(String orderNo) throws SQLException;

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    int insert(OrderItem orderItem) throws SQLException;

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 影响行数
     */
    int update(OrderItem orderItem) throws SQLException;

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id) throws SQLException;

    /**
     * 通过订单号删除数据
     *
     * @param orderNo 订单号
     * @return 是否成功
     */
    int deleteByOrderNo(String orderNo) throws SQLException;

}