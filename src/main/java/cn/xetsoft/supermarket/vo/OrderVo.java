package cn.xetsoft.supermarket.vo;

import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.util.BigDecimalUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单信息组合信息
 * @since 2020/12/21 14:58
 */

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class OrderVo {
    /**
     * 订单列表
     */
    private List<Order> orders;
    /**
     * 销售总额
     */
    private BigDecimal totalPrice = new BigDecimal(0);
    /**
     * 销售总利润
     */
    private BigDecimal totalProfit = new BigDecimal(0);


    public OrderVo(List<Order> orders) {
        this.orders = orders;
        for (Order order : orders) {
            totalPrice = BigDecimalUtil.add(totalPrice.doubleValue(),order.getTotalPrice().doubleValue());
            totalProfit = BigDecimalUtil.add(totalProfit.doubleValue(),order.getProfit().doubleValue());
        }
    }
}
