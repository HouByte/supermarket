package cn.xetsoft.supermarket;
import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.SettingService;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.view.*;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class SupermarketApplication {


    /**
     * 异步通知消息类
     */
    public static class CallResult{

        //标记结果
        private boolean flag;

        public CallResult() {
            this.flag = false;
        }

        public CallResult(boolean flag) {
            this.flag = flag;
        }

        public boolean isFlag() {
            return flag;
        }

        public void setFlag(boolean flag) {
            this.flag = flag;
        }
    }

    public static void main(String[] args) {
        GUITools.initStyle(new Font("alias", Font.PLAIN, 12));

        StartFrame.shows();

        SettingService settingService = Bean.getSettingServiceInstance();
        DataResponse dataResponse = settingService.initSystem();
        CallResult callResult = new CallResult(true);
        if (!dataResponse.isSuccess()){
            callResult.setFlag(false);
            //提示
            JOptionPane.showMessageDialog(null,"发现数据库异常,请先配置数据库");

            //跳转数据库设置页面
            SystemManagement systemManagement = new SystemManagement();
            systemManagement.setResult(callResult);

            //不让加载条继续显示
            StartFrame.hidden();
            StartFrame.TaskSuccess(0); //清空进度条
        }

        //如果系统错误,等待初始化完成
        while (!callResult.isFlag()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new LoginFrame();


    }
}
