package cn.xetsoft.supermarket.common;

/**
 * @author 侯国强
 * @since 2020/12/19 19:28
 * @version 1.0
 * @Description 代理类 数据包信息来传递类之间数据情况
 */
public class DataResponse<T>{

    // 响应业务状态
    private Integer code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private T data;

    //私有构造函数，禁止外部创建，需要使用提供构建方法
    private DataResponse() {
    }

    /**
     * 创建成功消息
     */
    public static <T> DataResponse<T> build(Integer status, String msg, T data) {
        return new DataResponse<T>(status, msg, data);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法只设置data
     * 其他默认
     */
    public static <T> DataResponse<T> success(T data) {
        return new DataResponse(data);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法不设置
     */
    public static <T> DataResponse<T> success() {
        return new DataResponse(null);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法只设置msg
     * 其他默认
     */
    public static <T> DataResponse<T> success(String msg) {
        return new DataResponse(200, msg, null);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法设置msg，data
     * 其他默认
     */
    public static <T> DataResponse<T> success(String msg, T data) {
        return new DataResponse(200, msg, data);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法设置code,msg
     * 其他默认
     */
    public static <T> DataResponse<T> success(int code, String msg) {
        return new DataResponse(code, msg, null);
    }

    /**
     * 创建成功消息
     * 重载多个
     * 本方法全部设置
     */
    public static <T> DataResponse<T> success(int code, String msg, T data) {
        return new DataResponse(200, msg, data);
    }

    /**
     * 创建错误消息
     * 重载多个
     * 本方法默认参数
     */
    public static <T> DataResponse<T> error() {
        return new DataResponse(500, "error", null);
    }

    /**
     * 创建错误消息
     * 重载多个
     * 本方法只设置msg
     * 其他默认
     */
    public static <T> DataResponse<T> error(String msg) {
        return new DataResponse(500, msg, null);
    }

    /**
     * 创建错误消息
     * 重载多个
     * 本方法设置code,msg
     * 其他默认
     */
    public static <T> DataResponse<T> error(int code, String msg) {
        return new DataResponse(code, msg, null);
    }

    /**
     * 创建错误消息
     * 重载多个
     * 本方法全部设置
     */
    public static <T> DataResponse<T> error(int code, String msg, T data) {
        return new DataResponse(code, msg, data);
    }

    /**
     * 创建错误消息
     * 重载多个
     * 本方法只设置data
     * 其他默认
     */
    public static <T> DataResponse<T> error(T data) {
        return new DataResponse(500, "error", data);
    }

    /**
     * 创建业务错误消息
     * 重载多个
     * 本方法设置msg
     * 其他默认
     */
    public static <T> DataResponse<T> errorTokenMsg(String msg) {
        return new DataResponse(400, msg, null);
    }

    /**
     * 创建异常消息
     * 重载多个
     * 本方法设置msg
     * 其他默认
     */
    public static <T> DataResponse<T> errorException(String msg) {
        return new DataResponse(555, msg, null);
    }

    /**
     * 创建异常消息
     * 重载多个
     * 本方法设置data
     * 其他默认
     */
    public static <T> DataResponse<T> errorException(T data) {
        return new DataResponse(555, "exception", data);
    }

    /**
     * 创建异常消息
     * 重载多个
     * 本方法设置msg,data
     * 其他默认
     */
    public static <T> DataResponse<T> errorException(String msg, T data) {
        return new DataResponse(555, msg, data);
    }

    //构造函数 所有参数
    public DataResponse(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    //构造函数 设置data其他默认
    public DataResponse(T data) {
        this.code = 200;
        this.msg = "Success";
        this.data = data;
    }

    //判断是否成功，状态码200成功
    public Boolean isSuccess() {
        return this.code == 200;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ServerResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

}
