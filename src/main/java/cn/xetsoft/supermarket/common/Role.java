package cn.xetsoft.supermarket.common;

/**
 * @author 侯国强
 * @since 2020/12/19 20:35
 * @version 1.0
 * @Description 角色枚举类
 */
public enum Role {
    ROLE_NO_AUTH(0,"未授权用户"),
    ROLE_CUSTOMER(1,"普通用户"),
    ROLE_ADMIN(2,"管理员"),
    ROLE_ROOT(3,"超级管理员");
    Role(int code, String value) {
        this.code = code;
        this.value = value;
    }

    private String value;
    private int code;

    public String getValue() {
        return value;
    }

    public int getCode() {
        return code;
    }

    /**
     * 提供code获取枚举值
     */
    public static Role codeOf(int code) {
        for (Role role : values()) {
            if (role.getCode() == code) {
                return role;
            }
        }
        throw new RuntimeException("没有找到对应的枚举");
    }

    /**
     * 提供values获取枚举值
     */
    public static Role rvalueOf(String value) {
        for (Role role : values()) {
            if (role.getValue().equals(value)) {
                return role;
            }
        }
        throw new RuntimeException("没有找到对应的枚举");
    }

    @Override
    public String toString() {
        return "Role{" +
                "value='" + value + '\'' +
                ", code=" + code +
                '}';
    }
}
