package cn.xetsoft.supermarket.common;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @author 侯国强
 * @version 1.0
 * @Description Excel需要枚举类转换
 * @since 2020/12/22 20:08
 */
public class SettlementConverter implements Converter<Role> {


    @Override
    public Class supportJavaTypeKey() {
        return null;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    /**
     * excel 内容转Role
     */
    @Override
    public Role convertToJavaData(CellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return Role.rvalueOf(String.valueOf(cellData));
    }

    /**
     *java类中对象枚举转换字符串
     */
    @Override
    public CellData convertToExcelData(Role value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {

        return  new CellData(value.getValue());
    }

}