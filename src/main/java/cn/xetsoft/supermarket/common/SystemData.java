package cn.xetsoft.supermarket.common;

import cn.xetsoft.supermarket.entity.User;

/**
 * @author 罗延剑
 * @version 1.0
 * @Description 系统全局信息类
 * @since 2020/12/24 22:08
 */
public class SystemData {
    //设置登入用户
    private static User user;

    public static User getUser() {
        if (user == null){
            System.out.println("用户当前尚未登入！");
        }
        return user;
    }

    public static void setUser(User user) {
        SystemData.user = user;
    }


}
