package cn.xetsoft.supermarket.util;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.util.Enumeration;

/*
 * 工具类
 */
public class GUITools {
    //JAVA提供的GUI默认工具类对象
    static Toolkit kit = Toolkit.getDefaultToolkit();
    //将指定组件屏幕居中
    public static void center(Component c) {
        int x = (kit.getScreenSize().width - c.getWidth()) / 2;
        int y = (kit.getScreenSize().height - c.getHeight()) / 2;
        c.setLocation(x, y);
    }
    //为指定窗口设置图标标题
    public static void setTitleImage(JFrame frame,String titleIconPath) {
        frame.setIconImage(kit.createImage(titleIconPath));
    }

    public static void initStyle(Font font){
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                /**
                 * Metal
                 * Nimbus
                 * CDE/Motif
                 * Windows
                 * Windows Classic
                 */
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
            FontUIResource fontRes = new FontUIResource(font);
            for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys.hasMoreElements();) {
                Object key = keys.nextElement();
                Object value = UIManager.get(key);
                if (value instanceof FontUIResource) {
                    UIManager.put(key, fontRes);
                }
            }
            }catch(Exception e) {
            System.out.println(e);
        }
    }
}
