package cn.xetsoft.supermarket.util;

import cn.xetsoft.supermarket.entity.Cart;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.entity.Order;
import cn.xetsoft.supermarket.entity.User;

import java.util.ArrayList;
import java.util.List;

public class ViewDataUtil {
    public String[][] GoodlistToArrays(List<Good> list){
        //根据Good的model与集合数据定义JTable的数据二维数组
        String[][] tbody = new String[list.size()][10];
        for (int i = 0; i < list.size(); i++) {
            Good goodItem = list.get(i);
            tbody[i][0] = String.valueOf(goodItem.getGid());
            tbody[i][1] = goodItem.getGoodname();
            tbody[i][2] = goodItem.getGoodbrand();
            tbody[i][3] = String.valueOf(goodItem.getSaleprice());
            tbody[i][4] = String.valueOf(goodItem.getInprice());
            tbody[i][5] = goodItem.getType();
            tbody[i][6] = String.valueOf(goodItem.getStock());
            tbody[i][7] = String.valueOf(goodItem.getCreateTime());
            tbody[i][8] = String.valueOf(goodItem.getIndate());
            tbody[i][9] = String.valueOf(goodItem.getUpdateTime());
        }
        return tbody;
    }

    public String[][] GoodlistToArraysByCashier(List<Good> list){
        //根据Good的model与集合数据定义JTable的数据二维数组
        String[][] tbody = new String[list.size()][8];
        for (int i = 0; i < list.size(); i++) {
            Good goodItem = list.get(i);
            tbody[i][0] = String.valueOf(goodItem.getGid());
            tbody[i][1] = goodItem.getGoodname();
            tbody[i][2] = goodItem.getGoodbrand();
            tbody[i][3] = String.valueOf(goodItem.getSaleprice());
            tbody[i][4] = goodItem.getType();
            tbody[i][5] = String.valueOf(goodItem.getStock());
            tbody[i][6] = String.valueOf(goodItem.getCreateTime());
            tbody[i][7] = String.valueOf(goodItem.getUpdateTime());
        }
        return tbody;
    }

    public String[][] UserlistToArrays(List<User> list){
        //根据Good的model与集合数据定义JTable的数据二维数组
        String[][] tbody = new String[list.size()][8];
        for (int i = 0; i < list.size(); i++) {
            User UserItem = list.get(i);
            tbody[i][0] = String.valueOf(UserItem.getId());
            tbody[i][1] = UserItem.getUsername();
            tbody[i][2] = UserItem.getName();
            tbody[i][3] = UserItem.getRole().getValue();
            tbody[i][4] = String.valueOf(UserItem.getPhone());
            tbody[i][5] = UserItem.getFrequency();
            tbody[i][6] = String.valueOf(UserItem.getCreateTime());
            tbody[i][7] = String.valueOf(UserItem.getUpdateTime());
        }
        return tbody;
    }

    public String[][] CartlistToArrays(ArrayList<Cart> list){
        //根据Good的model与集合数据定义JTable的数据二维数组
        String[][] tbody = new String[list.size()][5];
        for (int i = 0; i < list.size(); i++) {
            Cart goodItem = list.get(i);
            tbody[i][0] = String.valueOf(goodItem.getGoodbrand());
            tbody[i][1] = goodItem.getGoodname();
            tbody[i][2] = String.valueOf(goodItem.getSaleprice());
            tbody[i][3] = goodItem.getType();
            tbody[i][4] = String.valueOf(goodItem.getQuantity());
        }
        return tbody;
    }

    public String[][] OrderToArrays(ArrayList<Order> list){
        //根据Good的model与集合数据定义JTable的数据二维数组
        String[][] tbody = new String[list.size()][7];
        for (int i = 0; i < list.size(); i++) {
            Order orderitem = list.get(i);
            tbody[i][0] = String.valueOf(orderitem.getId());
            tbody[i][1] = String.valueOf(orderitem.getOrderNo());
            tbody[i][2] = orderitem.getDesc();
            tbody[i][3] = String.valueOf(orderitem.getTotalPrice());
            tbody[i][4] = String.valueOf(orderitem.getProfit());
            tbody[i][5] = String.valueOf(orderitem.getSaleTime());
            tbody[i][6] = String.valueOf(orderitem.getUpdateTime());
        }
        return tbody;
    }

}
