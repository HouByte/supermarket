package cn.xetsoft.supermarket.util;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 订单号生成工具
 * @since 2020/12/21 12:31
 */
public class OrderNoUtils {

    private static String lasttimestamp = "1";
    /**
     * 简单的 生成订单号
     */
    public synchronized static String nextNo(){
        //标记
        boolean flag = true;
        StringBuilder sb = null;
        while (flag) {
            Date now = new Date();
            //格式化时间当前
            String time = DateFormatUtils.format(now, "yyMMddHHmmssSSS");
            //获取时间戳
            long timestamp = now.getTime();
            //时间+时间戳+随机数
            sb = new StringBuilder(time).append(((int)Math.random()) % 100);
            if (lasttimestamp.compareTo(sb.toString()) != 0){
                flag = false;
            }
        }
        return sb.toString();
    }
}
