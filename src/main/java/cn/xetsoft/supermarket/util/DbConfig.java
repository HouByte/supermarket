package cn.xetsoft.supermarket.util;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 数据库配置信息
 * @since 2020/12/22 16:07
 */
public class DbConfig {
    //默认地址
    private String address = "localhost";
    //默认端口
    private Integer port = 3306;
    //数据库
    private String database;
    //用户名
    private String username;
    //密码
    private String password;

    public DbConfig() {
    }

    public DbConfig(String address, Integer port, String database, String username, String password) {
        this.address = address;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        init();
    }

    //数据库连接url
    private String url;

    public void init(){
//        url = "jdbc:mysql://"+address+":"+port+"/"+database+"?serverTimezone=UTC&characterEncoding=utf-8&useSSL=false";
        url = "jdbc:mysql://"+address+":"+port+"/?serverTimezone=UTC&characterEncoding=utf-8&useSSL=false&autoReconnect=true&failOverReadOnly=false&autoReconnectForPools=true";
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
