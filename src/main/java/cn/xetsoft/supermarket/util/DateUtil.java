package cn.xetsoft.supermarket.util;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 罗延剑
 * @version 1.0
 * @Description 日期 工具类
 * @since 2020/12/24 22:31:47
 */
public class DateUtil {
    public Date StringToDate(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
        Date DateResult = simpleDateFormat.parse(date);
        return DateResult;
    }

    public static String getNowDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static boolean isDate(String str) {
        boolean result = true;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            format.parse(str);
        }catch (Exception e){
            result = false;
        }
        return result;
    }
}
