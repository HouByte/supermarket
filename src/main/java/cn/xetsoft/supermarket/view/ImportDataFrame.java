package cn.xetsoft.supermarket.view;


import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.SettingService;
import cn.xetsoft.supermarket.util.DateUtil;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.event.*;
import java.util.Date;

/**
 * 文件导入页面
 */
@SuppressWarnings("serial")
public  class ImportDataFrame extends JFrame {
    //注册服务类
    SettingService settingService = Bean.getSettingServiceInstance();

    //组件
    private JButton generateBtn = new JButton("生成模板");//顾客按钮
    private JButton importBtn = new JButton("导入");//顾客按钮
    private JButton cancelBtn = new JButton("取消");//顾客按钮
    JTextField file = new JTextField();          // 登陆用户名
    private JCheckBox checkBox01 = new JCheckBox();
    private JCheckBox checkBox02 = new JCheckBox();
    private JCheckBox checkBox03 = new JCheckBox();
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    int xOld = 0;
    int yOld = 0;
    //构造函数
    public ImportDataFrame() {
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    //初始化操作
    private void init() {
        this.setTitle("导入数据 - 熵客云超市管理");// 标题
        this.setSize(600, 400);// 窗体大小与位置

        GUITools.center(this);//设置窗口在屏幕上的位置
//        GUITools.setTitleImage(this, "title.png");
        this.setResizable(false);// 窗体大小固定
//        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 关闭窗口默认操作

        //页面监听,让页面可以拖动
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xOld = e.getX();
                yOld = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int xOnScreen = e.getXOnScreen();
                int yOnScreen = e.getYOnScreen();

                int xx = xOnScreen - xOld;
                int yy = yOnScreen - yOld;
                ImportDataFrame.this.setLocation(xx, yy);
            }
        });
    }
    //添加组件
    private void addComponent() {


        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/ImportSQLBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//背景图
        //创建背景
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());
        jl=new JLabel(image);
        jp.add(jl);


        //控件页面定位
        generateBtn.setBounds(106,292,130,54);
        importBtn.setBounds(270,292,130,54);
        cancelBtn.setBounds(434,291,130,54);
        checkBox01.setBounds(57,158,50,50);
        checkBox02.setBounds(57,200,50,50);
        checkBox03.setBounds(57,242,50,50);
        file.setBounds(346,200,217,32);
        file.setText(DateUtil.getNowDate() + " " + PropertiesUtil.getProperty("excel.import.name"));
        checkBox01.setContentAreaFilled(false);
        checkBox02.setContentAreaFilled(false);
        checkBox03.setContentAreaFilled(false);
        //将背景图放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将控件放到高一层的地方
        layeredPane.add(generateBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(importBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(cancelBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox01,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox02,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox03,JLayeredPane.MODAL_LAYER);
        layeredPane.add(file,JLayeredPane.MODAL_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);


    }
    //添加监听器
    private void addListener(ImportDataFrame importDataFrame) {
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importDataFrame.setVisible(false);
            }
        });
        generateBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DataResponse dataTemplate = settingService.createDataTemplate(DateUtil.getNowDate()  + PropertiesUtil.getProperty("excel.import.name")); //从配置读入默认文件名进行生成
                if (dataTemplate.isSuccess()){
                    JOptionPane.showMessageDialog(null,"生成的模板已打开,保存后可以导入");
                }else {
                    JOptionPane.showMessageDialog(null,"生成模板失败!");
                }
            }
        });
        importBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                DataResponse dataResponse = settingService.initOrderData(file.getText());
                boolean goodData = checkBox01.isSelected(); //导入用户表选项
                boolean orderData = checkBox02.isSelected(); //导入商品表选项
                boolean userData = checkBox03.isSelected(); //导入订单选项
                boolean flag = true;
                if (!goodData && !orderData && !userData){ //如果都没有选中的情况
                    JOptionPane.showMessageDialog(null,"没有选中导出何种数据!");
                    return;
                }
                if (userData){
                    DataResponse dataResponse = settingService.initUserData(file.getText()); //导入用户表
                    if (!dataResponse.isSuccess()){
                        flag = false;
                    }
                }
                if (goodData){
                    DataResponse dataResponse = settingService.initGoodData(file.getText()); //导入商品表
                    if (!dataResponse.isSuccess()){
                        flag = false;
                    }
                }
                if (orderData){
                    System.out.println(file.getText());
                    DataResponse dataResponse = settingService.initOrderData(file.getText()); //导入订单表
                    if (!dataResponse.isSuccess()){
                        flag = false;
                    }
                    dataResponse = settingService.initOrderItemData(file.getText()); //导入订单物品表
                    if (!dataResponse.isSuccess()){
                        flag = false;
                    }
                }
                if (flag){
                    JOptionPane.showMessageDialog(null,"导入数据成功！请返回查看！");
                }else {
                    JOptionPane.showMessageDialog(null,"导入数据失败！请重新查看！");
                }
            }
        });
        file.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser chooser=new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); //点击弹出文件和文件夹选择框
                if(chooser.showOpenDialog(importDataFrame)==JFileChooser.APPROVE_OPTION){
                    System.out.println(chooser.getSelectedFile());
                    file.setText(String.valueOf(chooser.getSelectedFile()));
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });


    }

}
