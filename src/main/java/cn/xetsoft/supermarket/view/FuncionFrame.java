package cn.xetsoft.supermarket.view;

import cn.xetsoft.supermarket.common.SystemData;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;
import cn.xetsoft.supermarket.view.Goods.GoodsFrame;
import cn.xetsoft.supermarket.view.User.UserManagementFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * 主窗口类
 */
@SuppressWarnings("serial")
public class FuncionFrame extends JFrame {
    private String jurisdiction;
    //组件
    private JButton joinBtn = new JButton("收银台");//顾客按钮
    private JButton goodBtn = new JButton("商品管理");//顾客按钮
    private JButton userBtn = new JButton("用户管理");//顾客按钮
    private JButton orderBtn = new JButton("订单页面");//顾客按钮
    private JButton dbBtn = new JButton("数据库管理(_)");//顾客按钮
    private JButton quitBtn = new JButton("退出");//顾客按钮
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;
    private boolean keyFlag; //按下按键监听
    int xOld = 0;
    int yOld = 0;
    private String position;
    //构造函数
    public FuncionFrame() {
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    public  FuncionFrame(String position){
        this.position = position;
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    //初始化操作
    private void init() {
        this.setTitle("["+position+"]熵客云超市 操作员:【"+ SystemData.getUser().getName() +"】 权限:【"+SystemData.getUser().getRole().getValue()+"】 排班:【"+SystemData.getUser().getFrequency()+"】");// 标题
        this.setSize(600, 400);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 关闭窗口默认操作

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xOld = e.getX();
                yOld = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int xOnScreen = e.getXOnScreen();
                int yOnScreen = e.getYOnScreen();

                int xx = xOnScreen - xOld;
                int yy = yOnScreen - yOld;
                FuncionFrame.this.setLocation(xx, yy);
            }
        });
        User user = SystemData.getUser();
        System.out.println(user.getRole().getCode());
        jurisdiction = user.getRole().getValue();
    }
    //添加组件
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/FunctionBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//背景图
        //创建背景
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());

        jl=new JLabel(image);
		jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        jp.add(jl);

        joinBtn.setBounds(418, 33, 140, 41);
        goodBtn.setBounds(418, 90, 140, 41);
        userBtn.setBounds(418, 147, 140, 41);
        orderBtn.setBounds(418, 204, 140, 41);
        dbBtn.setBounds(418, 260, 140, 41);
        quitBtn.setBounds(418, 316, 140, 41);
        //将按钮添加到JPanel对象中

        //对用户权限进行限制
        if (jurisdiction.equals("普通用户")){
            goodBtn.setEnabled(false);
            userBtn.setEnabled(false);
            orderBtn.setEnabled(false);
            dbBtn.setEnabled(false);
        }else if (jurisdiction.equals("管理员")){
            dbBtn.setEnabled(false);
        }else if (jurisdiction.equals("未授权用户")){
            goodBtn.setEnabled(false);
            userBtn.setEnabled(false);
            orderBtn.setEnabled(false);
            dbBtn.setEnabled(false);
        }
        //将背景图放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将控件放到高一层的地方
        layeredPane.add(joinBtn,JLayeredPane.POPUP_LAYER);
        layeredPane.add(goodBtn,JLayeredPane.POPUP_LAYER);
        layeredPane.add(userBtn,JLayeredPane.POPUP_LAYER);
        layeredPane.add(orderBtn,JLayeredPane.POPUP_LAYER);
        layeredPane.add(dbBtn,JLayeredPane.POPUP_LAYER);
        layeredPane.add(quitBtn,JLayeredPane.POPUP_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(605,425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);

    }
    //添加监听事件 对页面进行跳转
    private void addListener(FuncionFrame funcionFrame) {
        joinBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new CashierFrame().setVisible(true);
            }
        });
        goodBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new GoodsFrame().setVisible(true);
            }
        });
        userBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new UserManagementFrame().setVisible(true);
            }
        });
        orderBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new OrderFrame();
            }
        });
        dbBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (keyFlag){
                    new LogViewFrame();
                }else {
                    new SystemManagement();
                }

            }
        });
        // KeyListener实现对CTRL键进行监听，如果按下设置KeyFlag，以便实现按住ctrl进入数据库设置
        dbBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 17){
                    dbBtn.setText("日志查看");
                    keyFlag = true;
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == 17){
                    dbBtn.setText("数据库管理");
                    keyFlag = false;
                }
            }
        });

        //推出
        quitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }
}
