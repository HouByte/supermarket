package cn.xetsoft.supermarket.view;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.dao.impl.UserDaoImpl;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.service.impl.UserServiceImpl;
import cn.xetsoft.supermarket.util.FileUtil;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;
import cn.xetsoft.supermarket.util.ViewDataUtil;
import cn.xetsoft.supermarket.view.FuncionFrame;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 日志页面
 */
@SuppressWarnings("serial")
public  class LogViewFrame extends JFrame{
    private UserService userService = Bean.getUserServiceInstance();

    //定义界面使用到的组件作为成员变量
    private JScrollPane tablePane = new JScrollPane();//滚动视口
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;
    private JTextArea textArea = new JTextArea();

    //构造方法

    public LogViewFrame() {
        this.init();// 初始化操作
        this.addComponent();// 添加组件
    }
    // 初始化操作
    private void init() {
        this.setTitle("超市日志");// 标题
        this.setSize(1286, 642);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
        LodingLogData(this);
    }
    // 添加组件
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/LogViewBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//背景图
        //创建背景
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());

        jl=new JLabel(image);
//		jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        jp.add(jl);

        //日志显示文本域
        textArea.setEnabled(false);
        tablePane.setViewportView(textArea);
        tablePane.setBounds(27, 108, 1233, 502);
        //将背景图放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将控件放到高一层的地方
        layeredPane.add(tablePane,JLayeredPane.MODAL_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),667);
        this.setVisible(true);

    }
    public void LodingLogData(LogViewFrame logViewFrame){
        /**
         * 在日志文件夹内查找
         */
        File file = new File("logs");
        //获取文件夹下文件列表
        File[] files = file.listFiles();
        //判断是否有文件
        if (files == null || files.length <= 0){
            logViewFrame.textArea.setText("没有日志");
            return;
        }
        //拿到最新的日志文件
        String filename = files[files.length-1].toString();
        if (filename == null){
            logViewFrame.textArea.setText("文件异常");
            return;
        }
        //拿到最新的配置信息
        int row = Integer.valueOf(PropertiesUtil.getProperty("debug.log.row"));
        //如果不存在就默认200行
        if (row <= 0){
            row = 200;
        }
        String s = null;
        try {
            /**
             * 读取后后row行日志
             */
            s = FileUtil.readLastRows(filename,null,row);
        } catch (IOException e) {
            logViewFrame.textArea.setText(e.getMessage());
        }
        //显示log
        logViewFrame.textArea.setText(s);
    }

}
