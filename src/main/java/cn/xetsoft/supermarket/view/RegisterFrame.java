package cn.xetsoft.supermarket.view;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.dao.impl.UserDaoImpl;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.service.impl.UserServiceImpl;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * 注册页面
 */
public class RegisterFrame extends JFrame {

    Color mainPaneColor = new Color(230, 230, 250);
    Color mainFrameColor = new Color(186, 110 ,64);
    Color logoFramColor = new Color(186, 110, 64);
    JLayeredPane layeredPane = new JLayeredPane();  // 面板层
    ImageIcon bgImage = new ImageIcon(Class.class.getResource("/Images/BG4.png").getFile());   // 背景图片
    ImageIcon lgImage  =new  ImageIcon(Class.class.getResource("/Images/RegIcon.png").getFile());
    JPanel bgPanel = new JPanel(); // 背景面板
    JPanel mainPanel = new JPanel(); // 登陆面板
    JPanel logoPanel  = new JPanel();//企业logo
    final JLabel Name = new JLabel("名称:");
    final JLabel user_name = new JLabel("用户名:");
    final JLabel user_password = new JLabel("密   码:");
    final JLabel user_phone = new JLabel("手机号:");
    JButton button_Register = new JButton("注册");// 确认按钮
    JButton button_cansel = new JButton("取消");//取消按钮
    JTextField text_username = new JTextField();          // 登陆名称
    JTextField text_name = new JTextField();          // 登陆用户名
    JPasswordField text_password = new JPasswordField();    // 登陆密码
    JTextField text_phone = new JTextField();    // 登陆密码
    private UserService userService = new UserServiceImpl(new UserDaoImpl());
    private static final long serialVersionUID = 1L;
    // 用于处理拖动事件，表示鼠标按下时的坐标，相对于JFrame
    int xOld = 0;
    int yOld = 0;


    //初始化方法
    public RegisterFrame() {
        super();
        initialize();
        this.addListener(this);// 添加监听器
    }

    //对页面进行了事件监听,添加控件
    public void initialize() {
        this.setTitle("系统注册 - 熵客云超市管理");
        this.setLayout(null);
        // 处理拖动事件
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xOld = e.getX();
                yOld = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int xOnScreen = e.getXOnScreen();
                int yOnScreen = e.getYOnScreen();

                int xx = xOnScreen - xOld;
                int yy = yOnScreen - yOld;
                RegisterFrame.this.setLocation(xx, yy);
            }
        });



        layeredPane.setBounds(0, 0, 600, 544);
        this.add(layeredPane);

        // 背景Panel
        bgPanel.setBounds(0, 0, 600, 544);
        layeredPane.add(bgPanel, new Integer(Integer.MIN_VALUE));

        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/BG4.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        bgImage = new ImageIcon(imgFile);

        // 背景图片，添加到背景Panel里面
        JLabel bgLabel = new JLabel(bgImage);
        bgPanel.add(bgLabel);
        JPanel jp = (JPanel) this.getContentPane();
        jp.setOpaque(false);



        //logo界面， 传播企业文化
        logoPanel.setBounds(0, 0,  170,  170);
        logoPanel.setLayout(null);
        logoPanel.setBackground(Color.WHITE);
        logoPanel.setBorder(BorderFactory.createTitledBorder(""));
        logoPanel.setBorder(BorderFactory.createLineBorder(logoFramColor));
        layeredPane.add(logoPanel);

        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile2 = "Images/RegIcon.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        lgImage = new ImageIcon(imgFile2);
        JLabel logo = new JLabel(change(lgImage,1.1));
        logoPanel.add(logo);
        logo.setBounds(new Rectangle(0, 0,  170,  170));


        // 主界面，登陆界面，包含服务器ip，用户名，密码等

        mainPanel.setBounds(150, 0, 350, 170);
        mainPanel.setLayout(null);
        mainPanel.setBackground(mainPaneColor);
        mainPanel.setBorder(BorderFactory.createTitledBorder(""));
        mainPanel.setBorder(BorderFactory.createLineBorder(mainFrameColor));
        layeredPane.add(mainPanel);

        mainPanel.add(Name);
        Name.setBounds(new Rectangle(25, 10, 62, 25));
        Name.setFont(new Font("微软雅黑", 1, 14));

        mainPanel.add(user_name);
        user_name.setBounds(new Rectangle(25, 40, 62, 25));
        user_name.setFont(new Font("微软雅黑", 1, 14));

        mainPanel.add(user_password);
        user_password.setBounds(new Rectangle(25, 70, 62, 25));
        user_password.setFont(new Font("微软雅黑", 1, 14));

        mainPanel.add(user_phone);
        user_phone.setBounds(new Rectangle(25, 100, 62, 25));
        user_phone.setFont(new Font("微软雅黑", 1, 14));



        mainPanel.add(text_name);
        text_name.setBounds(new Rectangle(95, 10, 240, 25));
        text_name.setFont(new Font("微软雅黑", 1, 12));


        mainPanel.add(text_username);
        text_username.setBounds(new Rectangle(95, 40, 240, 25));
        text_username.setFont(new Font("微软雅黑", 1, 12));

        mainPanel.add(text_password);
        text_password.setBounds(new Rectangle(95, 70, 240, 25));
        text_password.setFont(new Font("", Font.PLAIN, 30)); // 设置回显字符大小

        mainPanel.add(text_phone);
        text_phone.setBounds(new Rectangle(95, 100, 240, 25));
        text_phone.setFont(new Font("", Font.PLAIN, 30)); // 设置回显字符大小

        mainPanel.add(button_Register);
        button_Register.setBounds(new Rectangle(115, 130,  80, 25));
        button_Register.setFont(new Font("微软雅黑", 1, 12));

        mainPanel.add(button_cansel);
        button_cansel.setBounds(new Rectangle(225, 130,  80, 25));
        button_cansel.setFont(new Font("微软雅黑", 1, 12));


        this.setBounds(0, 0, 500, 170);
        this.setUndecorated(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }


    //对图片进行缩放
    public ImageIcon change(ImageIcon image,double i){//  i 为放缩的倍数

        int width=(int) (image.getIconWidth()*i);
        int height=(int) (image.getIconHeight()*i);
        Image img=image.getImage().getScaledInstance(width, height, Image.SCALE_DEFAULT);
        ImageIcon image2=new ImageIcon(img);

        return image2;

    }

    private void addListener(RegisterFrame registerFrame){
        button_Register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //读取页面上的文本框数据并且调用注册方法
                User user = new User();
                String name = registerFrame.text_name.getText();
                String username = registerFrame.text_username.getText();
                String pass = String.valueOf(registerFrame.text_password.getPassword());
                String phone = registerFrame.text_phone.getText();
                user.setName(name);
                user.setUsername(username);
                user.setFrequency("白班");
                user.setRole(Role.ROLE_CUSTOMER);
                user.setPhone(phone);
                user.setPassword(pass);
                DataResponse userDataResponse = userService.register(user); //注册默认的用户
                if (userDataResponse.isSuccess()){//成功，显示结果数据
                    String msg = userDataResponse.getMsg(); // 获取服务返回的信息
                    System.out.println("【成功】:"+ msg);
                    JOptionPane.showMessageDialog(registerFrame, msg, "注册成功",JOptionPane.WARNING_MESSAGE);
                    registerFrame.setVisible(false);
                    new LoginFrame().setVisible(true);
                } else { //失败查看错误信息
                    String msg = userDataResponse.getMsg();
                    System.out.println("【失败】:"+ msg);
                    JOptionPane.showMessageDialog(registerFrame, msg, "注册出错",JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        //取消按钮
        button_cansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new LoginFrame().setVisible(true);
                registerFrame.setVisible(false);
            }
        });
    }
}