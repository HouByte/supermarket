package cn.xetsoft.supermarket.view;

import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;


import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Timer;

/**
 * 启动闪图类
 */

public class StartFrame extends JFrame {
    private static JProgressBar jProgressBar;
    private BorderLayout borderLayout;
    private ImageIcon image;
    private JLabel jl;
    private static StartFrame startFrame;

    static {
        startFrame=new StartFrame();
        startFrame.initialize(); //初始化页面
        startFrame.setTitle("熵客云超市管理系统加载中...");
        startFrame.setSize(446, 270);// 窗体大小与位置
        GUITools.center(startFrame);//设置窗口在屏幕上的位置
        startFrame.setResizable(false);// 窗体大小固定
        startFrame.setUndecorated(true);
        startFrame.setVisible(false);
    }

    private StartFrame() {

    }

    public static void shows(){
        startFrame.setVisible(true);
    }

    public static void hidden(){
        startFrame.setVisible(false);
    }

    public void initialize() {
        borderLayout=new BorderLayout(); //使用BorderLayout容器很容易布局这类窗口
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/StartFrame.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//随便找一张图就可以看到效果。

        jl=new JLabel(image);
        jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        //进度条组件
        jProgressBar = new JProgressBar();
        jProgressBar.setBounds(0,0,image.getIconWidth(),image.getIconHeight());

        getContentPane().add(jl);
        getContentPane().add(jProgressBar,BorderLayout.SOUTH);

        this.setLocation(image.getIconWidth(),image.getIconHeight());
    }


    public static void TaskSuccess(int weight){
        if (jProgressBar.getValue() >= 100){
            //new LoginFrame();
            startFrame.setVisible(false);
            return;
        }
        jProgressBar.setValue(jProgressBar.getValue()+ weight);
        try {
            Thread.sleep(100L); //因为甲方觉得软件开启的太快,没有初始化的感觉,所以让线程休眠一段时间,充钱了那就无所谓了
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(jProgressBar.getValue());
    }



}
