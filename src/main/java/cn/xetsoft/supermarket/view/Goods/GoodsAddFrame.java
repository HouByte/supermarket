package cn.xetsoft.supermarket.view.Goods;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.SystemData;
import cn.xetsoft.supermarket.dao.impl.GoodDaoImpl;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.GoodService;
import cn.xetsoft.supermarket.service.impl.GoodServiceImpl;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.*;

/**
 * 商品添加窗口
 */
@SuppressWarnings("serial")
public  class GoodsAddFrame extends JDialog{
    private GoodsFrame goodsFrame; //商品功能页面
    GoodService goodService = Bean.getGoodServiceInstance();
    //添加功能组件
    protected JTextField addNumberText = new JTextField(6);//添加编号文本框
    protected JTextField addNameText = new JTextField(6);//添加名称文本框
    protected JTextField addbrandText = new JTextField(6);//添加名称文本框
    protected JTextField addInPriceText = new JTextField(6);//添加名称文本框
    protected JTextField addOutPriceText = new JTextField(6);//添加单价文本框
    protected JTextField addUnitText = new JTextField(6);//添加计价单位文本框
    private JTextField addInventoryText = new JTextField(6);//修改库存文本框
    private JTextField addInDate = new JTextField(6);//修改库存文本框
    private JButton addBtn = new JButton("添加商品");//添加按钮
    private JButton returnBtn = new JButton("返回");//添加按钮

    //背景图
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;


    /**
     * 商品添加页面初始化
     * @param goodsFrame
     */
    public GoodsAddFrame(GoodsFrame goodsFrame) {
        this.goodsFrame = goodsFrame;
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }



    // 初始化操作
    private void init() {
        this.setTitle("添加商品 - 超市货物管理");// 标题
        this.setSize(800, 500);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
    }


    /**
     * 添加控件
     */
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/GoodAddBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//背景图
        //创建背景
        jp=new JPanel();
        //背景定位
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());
        jl=new JLabel(image);
        jp.add(jl);

        //控件定位
        addNumberText.setBounds(169,122,200,20);
        addNumberText.setText("自动编号，无需填写");
        addNumberText.setEnabled(false); //禁止
        addNameText.setBounds(169,161,200,20);
        addbrandText.setBounds(169,198,200,20);
        addInPriceText.setBounds(169,239,200,20);
        addOutPriceText.setBounds(169,273,200,20);
        addUnitText.setBounds(169,312,200,20);
        addInventoryText.setBounds(460,122,100,20);
        addInDate.setBounds(460,161,100,20); //过期时间
        addBtn.setBounds(379,335,93,22);
        returnBtn.setBounds(478,335,93,22);

        //将背景放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);

        //将控件放到高一层的地方
        layeredPane.add(addNumberText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addNameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addbrandText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addInPriceText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addOutPriceText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addUnitText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addInventoryText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addInDate,JLayeredPane.MODAL_LAYER);
        layeredPane.add(returnBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addBtn,JLayeredPane.MODAL_LAYER);


        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);


    }
    // 添加监听器

    private void addListener(GoodsAddFrame goodsAddFrame) {
        //添加按钮监听
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //读取页面上文本框得数据并且添加到数据库
                Map<String,String> form = new HashMap<>();
                form.put("gid","0");
                form.put("goodname",addNameText.getText());
                form.put("goodbrand",addbrandText.getText());
                form.put("intime",String.valueOf(new Date().getTime()));
                form.put("saleprice",addOutPriceText.getText());
                form.put("inprice",addInPriceText.getText());
                form.put("type",addUnitText.getText());
                form.put("stock",addInventoryText.getText());
                form.put("indate",addInDate.getText());
                form.put("agent", SystemData.getUser().getName());
                System.out.println(form.toString());
                if (form.get("goodname").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品名称不能为空!");
                    return;
                }
                if (form.get("goodbrand").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品品牌不能为空!");
                    return;
                }
                if (form.get("saleprice").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品售价不能为空!");
                    return;
                }
                if (form.get("inprice").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品进价不能为空!");
                    return;
                }
                if (form.get("indate").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品过期时间不能为空!");
                    return;
                }
                if (form.get("type").equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null,"商品计价单位不能为空!");
                    return;
                }

                try {
                    Good good = new Good().mapToGood(form);
                    DataResponse<Good> listDataResponse = goodService.insert(good);
                    if (listDataResponse.isSuccess()){
                        JOptionPane.showMessageDialog(null,"添加商品成功！");
                        goodsFrame.LodingGood(goodsFrame.table);//刷新表格
                        goodsAddFrame.setVisible(false);
                    }else{
                        JOptionPane.showMessageDialog(null,"添加商品失败!" + listDataResponse.getMsg());
                    }
                } catch (ParseException parseException) {
                    parseException.printStackTrace();
                }

            }
        });

        //返回按钮
        returnBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                goodsAddFrame.setVisible(false);
            }
        });

    }

}
