package cn.xetsoft.supermarket.view.Goods;


import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.SystemData;
import cn.xetsoft.supermarket.entity.Good;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.GoodService;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;
import cn.xetsoft.supermarket.view.User.UserManagementFrame;
import lombok.SneakyThrows;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 管理窗口类
 */
@SuppressWarnings("serial")
public  class GoodsModifyFrame extends JDialog{
    //商品服务类
    private GoodService goodService = Bean.getGoodServiceInstance();
    //修改功能组件
    private JTextField updateNumberText = new JTextField(6);//修改编号文本框
    private JTextField updateNameText = new JTextField(6);//修改名称文本框
    private JTextField updateBrandText = new JTextField(6);//修改品牌文本框
    private JTextField updateInPriceText = new JTextField(6);//修改进价文本框
    private JTextField updateOutPriceText = new JTextField(6);//修改售价文本框
    private JTextField updateCompanyText = new JTextField(6);//修改计价单位文本框
    private JTextField updateInventoryText = new JTextField(6);//修改库存文本框
    private JTextField updateIndate = new JTextField(6);//修改库存文本框
    private JButton updateBtn = new JButton("修改商品");//修改按钮
    private JButton returnBtn = new JButton("返回");//添加按钮

    //背景图
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    //待修改的商品类
    private Good good;
    //商品框架
    private GoodsFrame goodsFrame;

    //构造方法
    public GoodsModifyFrame(Good good,GoodsFrame goodsFrame) {
        this.good = good;
        this.goodsFrame = goodsFrame;
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    // 初始化操作
    private void init() {
        this.setTitle("修改商品:"+good.getGoodname()+" - 超市货物管理");// 标题
        this.setSize(800, 500);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
    }
    // 添加组件
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/GoodModifyBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//背景图片。
        //创建背景
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());
        jl=new JLabel(image);
        jp.add(jl);

        //控件定位
        updateNumberText.setBounds(169,122,200,20);
        updateNameText.setBounds(169,161,200,20);
        updateBrandText.setBounds(169,198,200,20);
        updateInPriceText.setBounds(169,239,200,20);
        updateOutPriceText.setBounds(169,273,200,20);
        updateCompanyText.setBounds(169,312,200,20);
        updateInventoryText.setBounds(460,122,100,20);
        updateIndate.setBounds(460,161,100,20);
        updateBtn.setBounds(379,335,93,22);
        returnBtn.setBounds(478,335,93,22);

        //将背景图放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);

        //将控件放到高一层的地方
        layeredPane.add(updateNumberText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateNameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateBrandText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateInPriceText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateOutPriceText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateCompanyText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateInventoryText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateIndate,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(returnBtn,JLayeredPane.MODAL_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);//设置页面大小
        this.setLocation(image.getIconWidth(),image.getIconHeight());//设置页面显示的位置

        //将商品数据设置到页面上
        updateNumberText.setText(String.valueOf(good.getGid()));
        updateNameText.setText(good.getGoodname());
        updateBrandText.setText(good.getGoodbrand());
        updateOutPriceText.setText(String.valueOf(good.getSaleprice()));
        updateInPriceText.setText(String.valueOf(good.getInprice()));
        updateCompanyText.setText(good.getType());
        updateInventoryText.setText(String.valueOf(good.getStock()));


        this.setVisible(true);

    }
    // 添加监听器
    private void addListener(GoodsModifyFrame goodsModifyFrame) {
//        //修改按钮监听
        updateBtn.addActionListener(new ActionListener() {
            @SneakyThrows
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(1);
                //读取页面的内容,将修改的数据读取到数据库,并且刷新商品页面
                Map<String,String> form = new HashMap<>();
                form.put("gid", String.valueOf(good.getGid()));
                form.put("goodname",updateNameText.getText());
                form.put("goodbrand",updateBrandText.getText());
                form.put("saleprice",updateOutPriceText.getText());
                form.put("inprice",updateInPriceText.getText());
                form.put("type",updateCompanyText.getText());
                form.put("stock",updateInventoryText.getText());
                form.put("indate",updateIndate.getText());
                form.put("agent", SystemData.getUser().getName());
                DataResponse<Good> update = goodService.update(new Good().mapToGood(form));
                if (update.isSuccess()){
                    JOptionPane.showMessageDialog(null,"修改成功!");
                    goodsFrame.LodingGood(goodsFrame.table);
                    goodsModifyFrame.setVisible(false)
                    ;
                }else {
                    JOptionPane.showMessageDialog(null,"修改失败!");
                }
            }
        });

        //添加返回按钮监听事件
        returnBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //关闭商品修改页面
                goodsModifyFrame.setVisible(false);
            }
        });


    }

}
