package cn.xetsoft.supermarket.view;

import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * 主窗口
 */
@SuppressWarnings("serial")
public  class WelcomeFrame extends JFrame {
    //组件
    private JButton joinBtn = new JButton("进入系统");//顾客按钮
    private JButton quitBtn = new JButton("退出");//顾客按钮

    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    int xOld = 0;
    int yOld = 0;
    //构造函数
    public WelcomeFrame() {
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
//        this.setUndecorated(true);
    }
    //初始化操作
    private void init() {
        this.setTitle("熵云客超市管理系统 V1.0 Build");// 标题
        this.setSize(600, 400);// 窗体大小与位置

        GUITools.center(this);//设置窗口在屏幕上的位置
//        GUITools.setTitleImage(this, "title.png");
        this.setResizable(false);// 窗体大小固定
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 关闭窗口默认操作

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xOld = e.getX();
                yOld = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int xOnScreen = e.getXOnScreen();
                int yOnScreen = e.getYOnScreen();

                int xx = xOnScreen - xOld;
                int yy = yOnScreen - yOld;
                WelcomeFrame.this.setLocation(xx, yy);
            }
        });
    }
    //添加组件
    private void addComponent() {


        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/WelcomeBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//随便找一张图就可以看到效果。
        //创建背景的那些东西
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());

        jl=new JLabel(image);
//		jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        jp.add(jl);

        //创建一个测试按钮
        joinBtn.setBounds(355,106,202,61);
        quitBtn.setBounds(355,195,202,61);

        //将jp放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将jb放到高一层的地方
        layeredPane.add(joinBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(quitBtn,JLayeredPane.MODAL_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),image.getIconHeight());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);


    }
    //添加监听器
    private void addListener(WelcomeFrame welcomeFrame) {
        joinBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new LoginFrame();
                welcomeFrame.setVisible(false);
            }
        });
        quitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }
}
