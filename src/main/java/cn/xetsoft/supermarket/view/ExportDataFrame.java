package cn.xetsoft.supermarket.view;


import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.entity.ExportSelect;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.ExcelService;
import cn.xetsoft.supermarket.service.SettingService;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.*;
import java.io.File;

/**
 * 导出数据页面
 */
@SuppressWarnings("serial")
public  class ExportDataFrame extends JFrame {
    //注册excel服务类
    SettingService settingService = Bean.getSettingServiceInstance();

    //组件
    private JButton generateBtn = new JButton("生成模板");//顾客按钮
    private JButton importBtn = new JButton("导出");//顾客按钮
    private JButton cancelBtn = new JButton("取消");//顾客按钮
    JTextField file = new JTextField();          // 登陆用户名
    private JCheckBox checkBox01 = new JCheckBox();
    private JCheckBox checkBox02 = new JCheckBox();
    private JCheckBox checkBox03 = new JCheckBox();
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    int xOld = 0;
    int yOld = 0;
    //构造函数
    public ExportDataFrame() {
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
//        this.setUndecorated(true);
    }
    //初始化操作
    private void init() {
        this.setTitle("导出数据表 - 熵客云超市管理!");// 标题
        this.setSize(600, 400);// 窗体大小与位置

        GUITools.center(this);//设置窗口在屏幕上的位置

        this.setResizable(false);// 窗体大小固定
//        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 关闭窗口默认操作

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                xOld = e.getX();
                yOld = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int xOnScreen = e.getXOnScreen();
                int yOnScreen = e.getYOnScreen();

                int xx = xOnScreen - xOld;
                int yy = yOnScreen - yOld;
                ExportDataFrame.this.setLocation(xx, yy);
            }
        });
    }
    //添加组件
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/exportSQLBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(Class.class.getResource("/Images/imgFile.png").getFile());//背景图
        //创建背景的
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());
        jl=new JLabel(image);
        jp.add(jl);
        importBtn.setBounds(270,292,130,54);
        cancelBtn.setBounds(434,291,130,54);
        checkBox01.setBounds(57,158,50,50);
        checkBox02.setBounds(57,200,50,50);
        checkBox03.setBounds(57,242,50,50);
        file.setBounds(346,200,217,32);
        file.setText(FileSystemView.getFileSystemView().getHomeDirectory()+ "\\"+ PropertiesUtil.getProperty("excel.export.name"));
        checkBox01.setContentAreaFilled(false);
        checkBox02.setContentAreaFilled(false);
        checkBox03.setContentAreaFilled(false);

        checkBox01.setSelected(true);
        //将jp放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将jb放到高一层的地方
//        layeredPane.add(generateBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(importBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(cancelBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox01,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox02,JLayeredPane.MODAL_LAYER);
        layeredPane.add(checkBox03,JLayeredPane.MODAL_LAYER);
        layeredPane.add(file,JLayeredPane.MODAL_LAYER);

        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);


    }
    //添加监听器
    private void addListener(ExportDataFrame exportDataFrame) {
        generateBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportDataFrame.setVisible(false);
            }
        });
        importBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //获取选中框状态
                boolean isGood = checkBox01.isSelected();
                boolean isOrder = checkBox02.isSelected();
                boolean isUser = checkBox03.isSelected();
                //注册表格导出服务类
                DataResponse dataResponse = settingService.exportDbData(PropertiesUtil.getProperty("excel.export.name"), Role.ROLE_ROOT, new ExportSelect(isGood,isOrder,isUser));
                if (dataResponse.isSuccess()){
                    JOptionPane.showMessageDialog(null,"导出指定数据成功!");
                }else {
                    JOptionPane.showMessageDialog(null,"导出错误!" + dataResponse.getMsg());
                }
//                System.exit(0);
            }
        });
        file.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                //文件选择器
                JFileChooser chooser=new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory()+ "/"+ PropertiesUtil.getProperty("excel.export.name"));
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if(chooser.showSaveDialog(exportDataFrame)==JFileChooser.APPROVE_OPTION){
                    System.out.println(chooser.getSelectedFile());
                    file.setText(String.valueOf(chooser.getSelectedFile()));
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exportDataFrame.setVisible(false);
            }
        });
    }

}
