package cn.xetsoft.supermarket.view.User;


import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.common.SystemData;
import cn.xetsoft.supermarket.dao.impl.UserDaoImpl;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.service.impl.UserServiceImpl;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 管理窗口类
 */
@SuppressWarnings("serial")
public  class UserModifyFrame extends JDialog{
    private UserService userService = Bean.getUserServiceInstance();

    //定义界面使用到的组件作为成员变量
    private User user;
    private UserManagementFrame userManagementFrame;
    private JLabel tableLabel = new JLabel("用户列表");//水果列表标题
    private JScrollPane tablePane = new JScrollPane();//滚动视口
    protected JTable table = new JTable(); //水果列表
    JComboBox RoleJcombo =new JComboBox();    //创建JComboBox

    //修改功能组件
    protected JTextField updateUserNameText = new JTextField(24);//修改编号文本框
    protected JTextField updateNameText = new JTextField(24);//修改名称文本框
    protected JTextField updatePasswordText = new JTextField(24);//修改单价文本框
    protected JTextField updatePhoneText = new JTextField(24);//修改计价单位文本框
    protected JTextField updateFrequencyText = new JTextField(24);//添加单价文本框
    protected JTextField updateRoleText = new JTextField(24);//添加单价文本框
    private JButton updateBtn = new JButton("修改用户");//修改按钮
    private JButton returnBtn = new JButton("返回");//添加按钮

    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    public UserModifyFrame(UserManagementFrame userManagementFrame,User user) {
        this.user = user;
        this.userManagementFrame = userManagementFrame;
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    // 初始化操作
    private void init() {
        this.setTitle("用户修改 - 熵客云超市管理");// 标题
        this.setSize(800, 500);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
    }
    // 添加组件
    private void addComponent() {
        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/UserModifyBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//随便找一张图就可以看到效果。
        //创建背景的那些东西
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());

        jl=new JLabel(image);
//		jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        jp.add(jl);


        updateUserNameText.setBounds(181,134,200,20);
        updateUserNameText.setText(user.getUsername());
        updateUserNameText.setEnabled(false);
        updatePasswordText.setBounds(181,165,200,20);
        updateNameText.setBounds(181,198,200,20);
        updateNameText.setText(user.getName());
        updatePhoneText.setBounds(181,227,200,20);
        updatePhoneText.setText(user.getPhone());
        updateFrequencyText.setBounds(181,255,200,20);
        updateFrequencyText.setText(user.getFrequency());
        /**
         * 权限校验
         */
        int code = SystemData.getUser().getRole().getCode();
        RoleJcombo.addItem("普通用户");
        if (code == 3){
            RoleJcombo.addItem("管理员");
            RoleJcombo.setSelectedItem("普通用户");
        }
        RoleJcombo.setSelectedItem(user.getRole().getValue());
        RoleJcombo.setBounds(181,283,200,20);
        updateBtn.setBounds(378,334,94,23);
        returnBtn.setBounds(477,334,95,23);
        //将jp放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将jb放到高一层的地方
        layeredPane.add(updateUserNameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updatePasswordText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateNameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updatePhoneText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateFrequencyText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(RoleJcombo,JLayeredPane.MODAL_LAYER);
        layeredPane.add(updateBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(returnBtn,JLayeredPane.MODAL_LAYER);


        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);
    }
    // 添加监听器
    private void addListener(UserModifyFrame userModifyFrame) {
//        //修改按钮监听
        returnBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //调用修改方法
                userModifyFrame.setVisible(false);
            }
        });
        //修改按钮监听
        updateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //调用修改方法
                String UserName = userModifyFrame.updateUserNameText.getText();
                String PassWord = userModifyFrame.updatePasswordText.getText();
                String Name = userModifyFrame.updateNameText.getText();
                String Phone = userModifyFrame.updatePhoneText.getText();
                String Frequency = userModifyFrame.updateFrequencyText.getText();
                String role = userModifyFrame.RoleJcombo.getSelectedItem().toString();
                user.setName(Name);
                user.setFrequency(Frequency);
                System.out.println(Role.rvalueOf(role));
                user.setRole(Role.rvalueOf(role));
                user.setPhone(Phone);
                if (!PassWord.trim().equals("")){
                    user.setPassword(PassWord);
                }
                System.out.println(user.getId());
                //调用更新，得到数据代理类
                DataResponse userDataResponse = userService.update(user);
                if (userDataResponse.isSuccess()){//成功，显示结果数据
                    System.out.println("【成功】:"+userDataResponse.getMsg());
                    userManagementFrame.LodingUser(userManagementFrame.table); //刷新表格
                    userModifyFrame.setVisible(false);
                } else { //失败查看错误信息
                    System.out.println("【失败】:"+userDataResponse.getMsg());
                }

            }
        });

    }

}
