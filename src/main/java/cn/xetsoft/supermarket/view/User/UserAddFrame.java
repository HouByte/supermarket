package cn.xetsoft.supermarket.view.User;

import cn.xetsoft.supermarket.common.DataResponse;
import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.common.SystemData;
import cn.xetsoft.supermarket.dao.impl.UserDaoImpl;
import cn.xetsoft.supermarket.entity.User;
import cn.xetsoft.supermarket.service.Bean;
import cn.xetsoft.supermarket.service.UserService;
import cn.xetsoft.supermarket.service.impl.UserServiceImpl;
import cn.xetsoft.supermarket.util.GUITools;
import cn.xetsoft.supermarket.util.PropertiesUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 用户添加页面
 */
@SuppressWarnings("serial")
public  class UserAddFrame extends JDialog{
    //注册服务
    private UserService userService = Bean.getUserServiceInstance();

    //用户管理界面对象
    private UserManagementFrame userManagementFrame;

    //文本框
    protected JTextField addUsernameText = new JTextField(24);//修改编号文本框
    protected JTextField addNameText = new JTextField(24);//修改名称文本框
    protected JTextField addPasswordText = new JTextField(24);//修改单价文本框
    protected JTextField addPhoneText = new JTextField(24);//修改计价单位文本框
    protected JTextField addFrequencyText = new JTextField(24);//添加单价文本框
    JComboBox RoleJcombo =new JComboBox();    //创建JComboBox
    private JButton addBtn = new JButton("添加用户");//添加按钮
    private JButton returnBtn = new JButton("返回");//返回按钮


    //背景图
    private JLayeredPane layeredPane;
    private ImageIcon image;
    private JPanel jp;
    private JLabel jl;

    //构造方法
    public UserAddFrame(UserManagementFrame userManagementFrame) {
        this.userManagementFrame = userManagementFrame;
        this.init();// 初始化操作
        this.addComponent();// 添加组件
        this.addListener(this);// 添加监听器
    }
    // 初始化操作
    private void init() {
        this.setTitle("用户添加 - 超市用户管理");// 标题
        this.setSize(800, 500);// 窗体大小与位置
        GUITools.center(this);//设置窗口在屏幕上的位置
        this.setResizable(false);// 窗体大小固定
    }
    // 添加组件
    private void addComponent() {

        layeredPane=new JLayeredPane();
        /**
         * 得到路径区别开发环境和jar包
         */
        String imgFile = "Images/UserAddBgImg.png";
        if (!PropertiesUtil.getProperty("debug.resources.type").equals("jar")){
            imgFile = Class.class.getResource("/").getPath() + imgFile;
        }
        image=new ImageIcon(imgFile);//随便找一张图就可以看到效果。
        //创建背景的那些东西
        jp=new JPanel();
        jp.setBounds(0,-10,image.getIconWidth(),image.getIconHeight());

        jl=new JLabel(image);
//		jl.setBounds(0,0,image.getIconWidth(),image.getIconHeight());
        jp.add(jl);


        addUsernameText.setBounds(181,122,200,20);
        addPasswordText.setBounds(181,157,200,20);
        addNameText.setBounds(181,192,200,20);
        addPhoneText.setBounds(181,230,200,20);
        addFrequencyText.setBounds(181,266,200,20);

        /**
         * 权限校验
         */
        int code = SystemData.getUser().getRole().getCode();
        RoleJcombo.addItem("普通用户");
        if (code == 3){
            RoleJcombo.addItem("管理员");
            RoleJcombo.setSelectedItem("普通用户");
        }
        RoleJcombo.setBounds(181,303,200,20);
        addBtn.setBounds(378,334,94,23);
        returnBtn.setBounds(477,334,95,23);
        //将jp放到最底层。
        layeredPane.add(jp,JLayeredPane.DEFAULT_LAYER);
        //将jb放到高一层的地方
        layeredPane.add(addUsernameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addPasswordText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addNameText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addPhoneText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addFrequencyText,JLayeredPane.MODAL_LAYER);
        layeredPane.add(RoleJcombo,JLayeredPane.MODAL_LAYER);
        layeredPane.add(addBtn,JLayeredPane.MODAL_LAYER);
        layeredPane.add(returnBtn,JLayeredPane.MODAL_LAYER);


        this.setLayeredPane(layeredPane);
        this.setSize(image.getIconWidth(),425);
        this.setLocation(image.getIconWidth(),image.getIconHeight());
        this.setVisible(true);
    }
    // 添加监听器
    private void addListener(UserAddFrame userAddFrame) {
        //添加按钮监听
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String UserName = userAddFrame.addUsernameText.getText();
                String PassWord = userAddFrame.addPasswordText.getText();
                String Name = userAddFrame.addNameText.getText();
                String Phone = userAddFrame.addPhoneText.getText();
                String Frequency = userAddFrame.addFrequencyText.getText();
                String  role = userAddFrame.RoleJcombo.getSelectedItem().toString();
                System.out.println(UserName + "" +
                        PassWord+ "" +
                        Name+ "" +
                        Phone+ "" +
                        Frequency+ "" +
                        role);

                User user = new User();
                user.setName(Name);
                user.setUsername(UserName);
                user.setFrequency(Frequency);
                user.setRole(Role.rvalueOf(role));
                user.setPhone(Phone);
                user.setPassword(PassWord);
                DataResponse userDataResponse = userService.register(user);
                if (userDataResponse.isSuccess()){//成功，显示结果数据
                    System.out.println("【成功】:"+userDataResponse.getMsg());
                    JOptionPane.showMessageDialog(null,"【成功】:"+userDataResponse.getMsg()); // 提示用户
                    userManagementFrame.LodingUser(userManagementFrame.table);
                    userAddFrame.setVisible(false);
                } else { //失败查看错误信息
                    JOptionPane.showMessageDialog(null,"【失败】:"+userDataResponse.getMsg());
                    System.out.println("【失败】:"+userDataResponse.getMsg());
                }
            }
        });
        //修改按钮监听
        returnBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //调用修改方法
                userAddFrame.setVisible(false);
            }
        });
    }
}
