package cn.xetsoft.supermarket.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2020-12-20 21:11:47
 */
/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (Order)实体类
 */
/**
 * Excel 格式配置
 */
@ContentRowHeight(15) //设置内容高度
@HeadRowHeight(20) //没置标题高度
@ColumnWidth( 18)// 设置列宽
//头字体设置成20
@HeadFontStyle(fontHeightInPoints = 16,bold = true)
//内容字体设置成10
@ContentFontStyle(fontHeightInPoints = 12)

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class Order implements Serializable {
    private static final long serialVersionUID = 604137738408919266L;
    /**
    * 订单编号
    */
    @ExcelProperty("编号") //Excel 列名注释
    private Integer id;
    /**
    * 订单号
    */
    @ExcelProperty("订单号") //Excel 列名注释
    private String orderNo;
    /**
    * 缩略详情
    */
    @ExcelProperty("简略详情") //Excel 列名注释
    private String desc;
    /**
    * 订单总价
    */
    @NumberFormat("#.##")
    @ExcelProperty("总价") //Excel 列名注释
    private BigDecimal totalPrice;
    /**
    * 订单利润
    */
    @NumberFormat("#.##")
    @ExcelProperty("利润") //Excel 列名注释
    private BigDecimal profit;
    /**
    * 订单经办人
    */
    @ExcelProperty("经办人") //Excel 列名注释
    private String agent;
    /**
    * 订单销售时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("销售时间") //Excel 列名注释
    private Date saleTime;
    /**
     * 更新时间
     */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("更新时间") //Excel 列名注释
    private Date updateTime;
}