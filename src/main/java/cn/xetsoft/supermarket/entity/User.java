package cn.xetsoft.supermarket.entity;

import cn.xetsoft.supermarket.common.Role;
import cn.xetsoft.supermarket.common.SettlementConverter;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.io.Serializable;

/**
 * @Author Vincent Vic
 * @Date 2020/12/19 20:22
 * @Version 1.0
 * @Description  (User)实体类
 */
/**
 * Excel 格式配置
 */
@ContentRowHeight(15) //设置内容高度
@HeadRowHeight(20) //没置标题高度
@ColumnWidth( 18)// 设置列宽
//头字体设置成20
@HeadFontStyle(fontHeightInPoints = 16,bold = true)
//内容字体设置成10
@ContentFontStyle(fontHeightInPoints = 12)

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 188732156301312774L;
    /**
    * id
    */
    @ExcelProperty("编码") //Excel 列名注释
    private Integer id;
    /**
    * 用户名
    */
    @ExcelProperty("用户名") //Excel 列名注释
    private String username;
    /**
    * 密码
    */
    @ExcelIgnore //Excel 不转换注释 密码不透露
    private String password;
    /**
    * 名字
    */
    @ExcelProperty("姓名") //Excel 列名注释
    private String name;
    /**
    * 电话
    */
    @ExcelProperty("电话") //Excel 列名注释
    private String phone;
    /**
    * 班次
    */
    @ExcelProperty("班次") //Excel 列名注释
    private String frequency;
    /**
    * 1 员工 2 管理员
    */
    @ExcelProperty(value = "角色",converter = SettlementConverter.class)
    private Role role;
    /**
    * 创建时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("创建时间") //Excel 列名注释
    private Date createTime;
    /**
    * 更新时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("更新时间") //Excel 列名注释
    private Date updateTime;

    public User(String username, String password, String name, String phone, String frequency, Role role) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.frequency = frequency;
        this.role = role;
    }
}