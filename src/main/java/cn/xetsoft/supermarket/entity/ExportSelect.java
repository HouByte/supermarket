package cn.xetsoft.supermarket.entity;

/**
 * @author 侯国强
 * @version 1.0
 * @Description 导出数据选择
 * @since 2020/12/22 20:46
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class ExportSelect {
    /**
     * 判断是否导出商品数据
     */
    private boolean isGood;
    /**
     * 判断是否导出订单数据（包含订单商品）
     */
    private boolean isOrder;
    /**
     * 判断是否导出用户数据
     */
    private boolean isUser;
}
