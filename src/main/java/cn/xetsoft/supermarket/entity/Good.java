package cn.xetsoft.supermarket.entity;

import cn.xetsoft.supermarket.util.DateUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.FillPatternType;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.Serializable;
import java.util.Map;

/**
 * @author 侯国强
 * @version 1.0
 * @Description  (Good)实体类
 * @since 2020/12/20 15:12
 */

/**
 * Excel 格式配置
 */
@ContentRowHeight(15) //设置内容高度
@HeadRowHeight(20) //没置标题高度
@ColumnWidth( 18)// 设置列宽
//头字体设置成20
@HeadFontStyle(fontHeightInPoints = 16,bold = true)
//内容字体设置成10
@ContentFontStyle(fontHeightInPoints = 12)

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class Good implements Serializable {
    private static final long serialVersionUID = 412976805611415159L;

    /**
     * 编码
     */
    @ExcelProperty("编号")
    private Integer gid;
    /**
    * 商品名
    */
    @ExcelProperty("商品名")
    private String goodname;
    /**
    * 商品品牌
    */
    @ExcelProperty("商品品牌")
    private String goodbrand;
    /**
    * 商品进货时间
    */
    @ExcelProperty("进货时间")
    private Date intime;
    /**
    * 商品销售时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("售货时间")
    private Date saletime;
    /**
    * 商品进价
    */
    @NumberFormat("#.##")
    @ExcelProperty("商品进价")
    private BigDecimal inprice;
    /**
    * 商品售价
    */
    @NumberFormat("#.##")
    @ExcelProperty("商品售价")
    private BigDecimal saleprice;
    /**
    * 计价单位
    */
    @ExcelProperty("计价单位")
    private String type;

    /**
     * 商品过期时间
     */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("过期时间")
    private Date indate;

    /**
    * 商品库存
    */
    @ExcelProperty("商品库存")
    private Integer stock;
    /**
    * 商品经办人
    */
    @ExcelProperty("经办人")
    private String agent;
    /**
    * 创建时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("更新时间")
    private Date updateTime;

    /**
     * map 转换称实体对象
     * by 罗延剑
     */
    public Good mapToGood(Map<String,String> map) throws ParseException {
        this.setGid(Integer.valueOf(map.get("gid")));
        this.setGoodname(map.get("goodname"));
        this.setGoodbrand(map.get("goodbrand"));
//        this.setIntime(new Date(Long.parseLong(map.get("intime"))));
        this.setInprice(new BigDecimal(map.get("inprice")));
        this.setSaleprice(new BigDecimal(map.get("saleprice")));
        this.setType(map.get("type"));
        this.setStock(Integer.valueOf(map.get("stock")));
        this.setAgent(map.get("agent"));
        this.setIndate(new SimpleDateFormat("yyyy-MM-dd").parse(map.get("indate")));
        return this;
    }
}