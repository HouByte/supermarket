package cn.xetsoft.supermarket.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;


/**
 * @author 侯国强
 * @since 2020/12/20 21:11:47
 * @version 1.0
 * @Description (OrderItem)实体类
 */
/**
 * Excel 格式配置
 */
@ContentRowHeight(15) //设置内容高度
@HeadRowHeight(20) //没置标题高度
@ColumnWidth( 18)// 设置列宽
//头字体设置成20
@HeadFontStyle(fontHeightInPoints = 16,bold = true)
//内容字体设置成10
@ContentFontStyle(fontHeightInPoints = 12)

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data

public class OrderItem implements Serializable {
    private static final long serialVersionUID = -67902889936953276L;
    /**
    * 订单商品项目编号
    */
    @ExcelProperty("编码") //Excel 列名注释
    private Integer id;
    /**
     * 订单号
     */
    @ExcelProperty("订单号") //Excel 列名注释
    private String orderNo;
    /**
    * 商品名
    */
    @ExcelProperty("商品名称") //Excel 列名注释
    private String goodname;
    /**
    * 商品品牌
    */
    @ExcelProperty("商品品牌") //Excel 列名注释
    private String goodbrand;
    /**
    * 商品售价
    */
    @NumberFormat("#.##")
    @ExcelProperty("销售价格") //Excel 列名注释
    private BigDecimal salePrice;
    /**
    * 计价单位
    */
    @ExcelProperty("计价单位") //Excel 列名注释
    private String type;
    /**
    * 商品数量
    */
    @ExcelProperty("商品数量") //Excel 列名注释
    private Integer quantity;
    /**
    * 创建时间
    */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("创建时间") //Excel 列名注释
    private Date createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty("更新时间") //Excel 列名注释
    private Date updateTime;
}