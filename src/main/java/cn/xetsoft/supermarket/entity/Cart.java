package cn.xetsoft.supermarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车数据
 */

/**
 * 注明：开发环境需要安装lombok插件，否则get,set会报红
 */
//使用Lombok注解，创建无参构造还是
@NoArgsConstructor
//使用Lombok注解，创建有参构造函数
@AllArgsConstructor
//使用Lombok注解，生成get,set,tostrin方法
@Data
public class Cart {
    /**
     * 商品名
     */
    private String goodname;
    /**
     * 商品品牌
     */
    private String goodbrand;
    /**
     * 商品进价
     */
    private BigDecimal inprice;
    /**
     * 商品售价
     */
    private BigDecimal saleprice;
    /**
     * 计价类型
     */
    private String type;

    /**
     * 商品经办人
     */
    private String agent;
    /**
     * 商品数量
     */
    private Integer quantity;
}
