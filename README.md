# 熵客云超市管理系统



## 一、	选题项目设计思想

  我组选择的项目选题是《超市管理系统》。我组将软件命名为：熵客云超市管理系统
现如今，我国超市经营规模日趋扩大，销售额和门店数量大幅度增加，而且许多超市正在突破以食品为主的传统格局，向品种多样化发展。小型超市在业务上需要处理大量的库存信息，还要时刻更新产品的销售信息，不断添加商品信息，并对商品各种信息进行统计分析。
因此，面对庞大的控制和记录需求，纸质存档已变得不现实，需要开发制作一款超市管理系统进而实现对超市庞大商品的控制和记录，从而方便销售行业的管理和决策，为超市和超市管理人员解除销售数据难以管理等痛点的后顾之忧。
我组基于选题《超市管理系统》开发的熵客云超市管理系统的目的在于开发一款好的超市销售管理系统，帮助销售部门提高工作效率，帮助超市工作人员利用计算机，极为方便的对超市的有关数据进行管理、输入、输出、查找等有关操作，使杂乱的超市数据能够具体化、直观化、合理化等。



## 二、	软件环境信息及依赖




|  模块化   | 详情  |
|  ----  | ----  |
| 开发环境  | Windows + Maven +Java8 + Git |
| 前端技术栈  | 基于Java语言Swing界面设计，使用IntelliJ IDEA 进行开发 |
| 后端技术栈  | 基于Java语言Maven项目管理，使用IntelliJ IDEA 进行开发 |
| 存储技术栈  | 基于Mysql关系型数据库管理系统与JDBC技术进行数据读写 |
| 运行环境  | 理论支持安装有Java的WindowsXP/Win7/Win8/Win10 系统下运行 |
| 开发模式  | 三层模型 |
| 开发团队  | 罗延剑、侯国强、刘建龙 |



## maven依赖外部功能

```pom
<dependencies>
        <!-- 数据库操作包 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.47</version>
        </dependency>
        <!-- 测试组件 -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.7.0</version>
            <scope>test</scope>
        </dependency>

        <!--格式化工具包 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.6</version>
        </dependency>
        <!--表格工具 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>easyexcel</artifactId>
            <version>2.2.6</version>
        </dependency>

        <!--通过简单的注解形式来帮助我们简化消除一些必须有但显得很臃肿的Java代码的工具 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.16</version>
        </dependency>
        <!-- log -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.21</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.1.7</version>
        </dependency>


    </dependencies>
```

# 三、实践目标及任务要求

**1****．实践目标**

按要求完成一个Java语言实现的超市管理系统并且实现下文实践内容中的功能要求，并编写实践报告（文档）。

**2****．任务要求**

①界面: 界面设计友好美观，页面总数不少于6页，代码要有50%的注释。

②所学技术: 功能完整性、运用技术合理，运行流畅，无逻辑错误，系统要具备良好的可扩展性与可维护性（使用继承、接口、多态等）。

连接数据库技术: 运用JDBC技术访问数据库。 

③答辩: 答辩态度、应变能力、解决问题的能力。

④文档: 项目总结报告。

# 四、实践内容及过程

​    **（一）功能设计**

![img](src/main/resources/md-img/功能模块图.png)
 超市管理系统会因为超市商品的数量、种类、提供的操作等不同而具有不同的复杂度。基本信息的维护、商品入库、销售及查询等操作通常是超市管理系统的基本功能。在规模较大、业务较多的超市还需要多店、收银等更加复杂的功能。

图1.1 软件功能模块图

 

为了增强用户的可操作性，将各个模块进行拓展，整理为以下需求思维导图。

 

 

 

系统管理模块主要是提供数据库、软件、日志三大块内容的管理和配置，方便用户修改变更数据库，便捷操作清空数据库、重建数据库等要求，系统设置使得软件更加灵活和可自定义性。

 

 

 

|      |                                                              |
| ---- | ------------------------------------------------------------ |
|      | ![img](src/main/resources/md-img/系统管理模块功能设计导图.png) |


图1.2 系统管理模块功能设计导图



 

  进销存模块主要实现对超市的进、销、存管理功能，通过进销存模块实现超市库存管理，销售管理和进货管理，使超市的管理井然有序，更加便捷。

 

|      |                                                              |
| ---- | ------------------------------------------------------------ |
|      | ![img](src/main/resources/md-img/进销存模块功能设计导图.png) |


图1.3 进销存模块功能设计导图



 

 

|      |                                                              |
| ---- | ------------------------------------------------------------ |
|      | ![img](src/main/resources/md-img/店面与操作员管理模块功能设计导图.png) |


  店面与操作员管理模块主要实现对店面、员工关系的管理，使总管理员可以查看和管理分店店员和分店管理员，实现分管功能。



图1.4 店面与操作员管理模块功能设计导图



## 软件代码实现设计

![软件设计](src/main/resources/md-img/熵客云软件设计图.jpg)

​											图1.5 软件代码实现设计图

    通过三层模型对系统进行设计，数据库操作dao，业务功能（功能校验，组合操作）service，页面功能view，实现各个层次功能职责明确，界面层与服务层之间交互创建对象交由创建的单例模式的Bean工具处理，减少业务改动影响多个类，创建代理类明确业务类操作结果和状态，调用第三方日志记录业务情况

 

**（二）权限设计**

| 角色      功能 | 进销存 | 收银 | 系统设置     |
| -------------- | ------ | ---- | ------------ |
| 店员           | 查看   | 可用 | 不可用       |
| 分店管理员     | 可用   | 可用 | 分店设置可用 |
| 总管理员       | 可用   | 可用 | 可用         |

* 查看：只允许查看数据，不提供增/删/改

* 可用：可以使用所有功能

* 不可用：该功能不会出现在界面上且该角色无法使用

* 部分可用： 只允许使用该模块中的部分功能

​        在本软件权限设计中，店员属于可用查看库存、查看销售情况并且可以使用销售收银台功能的员工，该角色只允许查看进销存数据但不可进行其他操作；分店管理员角色可用查看管理该分店下的进销存数据，同时可管理其分店下的员工，该角色不允许操作其他分店的进销存数据和员工；总管理员可对整个软件都具有操作权限。

**（三）数据库设计**

 

 

**（四）代码实现**

 

 

# 五、实践总结

通过该实践《超市管理系统》选题，我组实现了编写熵客云超市管理系统，通过该系统解决了关于超市的电子化问题，实现超市计算机进销存管理，使数据更加直观，并且方便管理，配备收银台功能，解决销售问题。在实现系统上，我们小组分工明确，通过对项目需求分析，制定开发模块，设定角色权限历经设计、开发、调试、测试等阶段最终完成开发。本程序将面向对象程序设计中的知识点均有覆盖，在开发过程中，也遇到了很多的麻烦和问题，例如：在定义对象时，前期没有考虑好，后续在开发时发现对象冗余问题较大，导致部分返工修改，经过讨论后重新编写，经过配合和讨论，集思广益后就将该问题尽可能解决，不重复返工。

实践作业考核让我们的技能不断增长，应变工作能力和自学能力都得以不断加强。刚开始编写的时候，发现自己学习的知识很死，知识面很窄，以前做的练习项目的实用性也不是很好。这次的实践作业考核通过具体的业务开发，让我们学以致用，知道所学的知识在实际中的作用。在项目开发过程中一边是队友们的悉心指导，一边是大家反复琢磨与理解，经过超市管理系统的开发，已经能够比较熟练的掌握基本的工作方法和一些技巧， 而且大家都能够独立完成一些模块的开发并且配合其中。

通过本次实践，组员一致觉得解决实际问题的能力得到了很好的锻炼。工作中也遇到了很多的以前没有遇到过的新技术，面对技术难题都总是直接面对，为了实现，没有逃避，也因此自学了好多新的技术，加深了对自己工作要负责的信念，并且从这个项目不仅学习到了很多技术也了解了整个项目的大体流程， 从需求分析、数据库设计、详细设计、代码编写、测试、项目维护等方面， 使自己不仅从一个代码编写人员的角度还从一个整体的角度来看整个项目开发， 加深了软件开发概念的理解。

在这次期末实践开发制作熵客云超市管理系统的过程中所见所学，都将为未来的路打下坚实的基础，在未来的学习生活中可以将经验学以致用，另外再次感谢实习期间各位老师的指导教诲和实践机会，你们给我们的知识财富将让我们受益终生。